/*

    About:
    
        This file is a place holder for the one day optimizing of the code trees that
        are constructed from the scanning of the program's parse trees. This level of optimization
        is independent of the target architecture so it will stay here.

*/

use crate::code_tree;

pub (crate) fn optimize(_trees: &mut Vec<code_tree::CodeTree>) {

    /*
    
            Put nice code here
    
    */
}