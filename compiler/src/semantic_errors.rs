use crate::parse_tree;
use crate::code_tree;

#[derive(Debug, Clone)]
pub enum SemanticError {

    NoEntryFunction,

    DuplicateFunctionDeclaration{ 
        name: String, 
        first_appearance: parse_tree::Location, 
        first_file: String, 
        second_appearance: parse_tree::Location,
        second_file: String
    },

    DuplicateStructureDeclaration{ 
        name: String, 
        first_appearance: parse_tree::Location, 
        first_file: String, 
        second_appearance: parse_tree::Location,
        second_file: String
    },

    DuplicateVariableDeclaration{ 
        name: String, 
        first_appearance: parse_tree::Location, 
        first_file: String, 
        second_appearance: parse_tree::Location,
        second_file: String
    },

    EmbeddedStruct{
        name: String,
        location: parse_tree::Location,
        file: String
    },

    UnknownIdentifier{
        name: String,
        location: parse_tree::Location,
        file: String
    },

    UnknownFunctionCall{
        name: String,
        location: parse_tree::Location,
        file: String
    },

    UnknownStruct{
        name: String,
        location: parse_tree::Location,
        file: String
    },

    MismatchedParametersLen{
        function_name: String,
        expected: usize, 
        actual: usize,
        location: parse_tree::Location,
        file: String
    },

    MismatchedStructExprLen{
        structure_name: String,
        expected: usize, 
        actual: usize,
        location: parse_tree::Location,
        file: String
    },

    MismatchedParameterType{
        function_name: String,
        expected: parse_tree::Types, 
        actual:   parse_tree::Types,
        location: parse_tree::Location,
        file: String
    },

    MismatchedStructParameterType{
        structure_name: String,
        expected: parse_tree::Types, 
        actual:   parse_tree::Types,
        location: parse_tree::Location,
        file: String
    },

    ExpressionOfInvalidAssignedType {
        variable_name: String,
        evaluated_type: parse_tree::Types,
        assigned_type:  parse_tree::Types,
        location:   parse_tree::Location,
        file: String
    },

    MismatchedFunctionReturnType{
        function: String,
        evaluated_return_type: parse_tree::Types,
        assigned_return_type:  parse_tree::Types,
        location: parse_tree::Location,
        file: String
    },

    InvalidAccess{
        item:   String,
        location: parse_tree::Location,
        file: String
    },

    InvalidAccessDepth{
        item:   String,
        expected_depth: usize,
        actual_depth:   usize,
        location: parse_tree::Location,
        file: String
    },

    UnknownMember{
        item: String,
        member: String,
        location: parse_tree::Location,
        file: String
    },

    DuplicateStructureMembers{
        item: String,
        location: parse_tree::Location,
        file: String
    },

    InvalidOperation{
        operation: parse_tree::Opcode,
        data_type: parse_tree::Types,
        location: parse_tree::Location,
        file: String
    },

    InvalidExpression{
        types_present: Vec<code_tree::VarTypes>,
        location: parse_tree::Location,
        file: String
    },

    InvalidFunctionCallNilExpression{
        location: parse_tree::Location,
        file: String
    },

    NeglectedFunctionReturnData {
        function_name: String,
        return_type:   parse_tree::Types,
        location: parse_tree::Location,
        file: String
    },

    ReturnStatementNotDetectedForFunction {
        function_name: String,
        return_type: parse_tree::Types,
        location: parse_tree::Location,
        file: String
    },

    InvalidConditional{ 
        present: code_tree::VarTypes,
        location: parse_tree::Location,
        file: String
    }
}