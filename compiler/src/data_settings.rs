

pub (crate) const WORD_SIZE   : usize = 8;
pub (crate) const HALF_SIZE   : usize = 4;
pub (crate) const BYTE_SIZE   : usize = 1;


pub (crate) const NIL_SIZE    : usize = 0;
pub (crate) const INTEGER_SIZE: usize = WORD_SIZE;
pub (crate) const FLOAT_SIZE  : usize = WORD_SIZE;
pub (crate) const CHAR_SIZE   : usize = HALF_SIZE;