/*

    About:

        This file iterates over a vector of code_trees and generates pseudo ASM instructions from the code_gen crate. This is really just a big enum that abstracts the actual ASM instructions. 
        Once the iterating and generating is completed, the instructions are sent off to code_gen for optimization and assembling.  

    Development Notes: 

        Optimizations: 
            The way that for loops and while are constructed leaves them with a vector of boxed statements. This means that in this file we have to
            iterate over them to create non-boxed statements for processing. Twice. This is not optimal. This needs to change. One thing that can be done is
            we can change the definition in the code_tree file, and all associated code that touches it, or, we can do it once per loop and save it off. 
            I'd prefer the former.

*/

use crate::code_tree;
use crate::code_gen;
use crate::data_settings;
use crate::verbosity;

use derive_more::{ Display };

use std::collections::HashMap;

#[derive(Debug, Display, Clone)]
pub (crate) enum GeneratorErrors {

    #[display(fmt = "Undefined Reassignment : {}", variable)]
    UndefinedReassignment{ variable: code_tree::Variable },
}

struct ProgramDefinitions {
    structure_definitions: HashMap<String, code_tree::Structure>,
    function_definitions:  HashMap<String, code_tree::Function>,
}

#[derive(Debug)]
struct FunctionLayout {
    stack_index: usize,
    function_name: String,
    instructions: Vec<code_gen::Instructions>,
    variables:   Vec<HashMap<String, usize>>,             // Stack index
    label_id:    u128
}

impl FunctionLayout {

    fn new(name: &String) -> Self {
        Self{
            stack_index: 0,
            function_name: name.clone(),
            instructions: Vec::new(),
            variables: Vec::new(),
            label_id:  0
        }
    }
    
    fn get_label(&mut self) -> String {
        let label = format!("__generated_lvm_label_function_{}_count_{}", self.function_name, self.label_id);
        self.label_id += 1;
        return label;
    }

    fn new_variable_context(&mut self) {
        self.variables.push(HashMap::new());
    }

    fn pop_context(&mut self) {
        self.variables.pop();
    }

    fn insert_variable(&mut self, name: &String, location: usize) {

        if self.variables.is_empty() {
            self.new_variable_context();
        }

        if let Some(ctx) = self.variables.last_mut() {

            ctx.insert(name.clone(), location);

        } else {
            panic!("Internal Error: Somehow a variable context wasn't created. This is bad");
        }
    }

    fn get_variable(&self, name: &String) -> Option<usize> {

        for ctx in self.variables.iter().rev() {
            if let Some(location) = ctx.get(name) {
                return Some(location.clone());
            }
        }

        None
    }
}

pub (crate) fn generate_lvm_bytecode(v: &verbosity::Verbosity, trees: &Vec<code_tree::CodeTree>) -> Result<Vec<u8>, GeneratorErrors> {

    let mut definitions = ProgramDefinitions{
        structure_definitions: HashMap::new(),
        function_definitions : HashMap::new()
    };

    //  We place all the definitions for things in a hashmap. Its copying the data, but it will make it quicker
    //  to reference it if we need to
    //
    for tree in trees.iter() {
        for structure in tree.structures.iter(){ 
            definitions.structure_definitions.insert(
                structure.name.clone(), structure.clone()
            ); 
        }
        for function in tree.functions.iter(){
            definitions.function_definitions.insert(
                function.name.clone(), function.clone()
            ); 
        }
    }

    let mut instruction_set : Vec<code_gen::Instructions> = Vec::new();

    //  Iterate over the trees again and generate the functions
    //
    for tree in trees.iter() {

        for function in tree.functions.iter() {

            //  Calculate the required size of the function
            //
            let required_function_size = calculate_function_size(&function);
            
            //  Function layout information
            //
            let mut function_layout = FunctionLayout::new(&function.name);

            vhigh!(v, vfunc!(), format!("Generating function '{}', params '{}'", function.name.clone(), function.parameters.len()).as_str());

            //  Comment the function
            //
            function_layout.instructions.push(
                code_gen::Instructions::Comment{ data: format!("Function: {} | Origin File: {} ", function.name.clone(), tree.file.clone()) }
            );
            function_layout.instructions.push(
                code_gen::Instructions::Comment{ data: String::from("") }
            );

            //  Label the start of the function
            //
            function_layout.instructions.push(
                code_gen::Instructions::Label{ name: function.name.clone() }
            );


            /*
            
                    Here we mark the stack and then expand it over the calculated size of the function. 
                    We mark and expand so we have the room we need, and we use the marker in various places to reference
                    the relative index from the function start in the stack to access variables. 
    
            */

            //  Mark the stack !! - This will move the stack frame pointer for the new function
            //
            function_layout.instructions.push(code_gen::Instructions::MarkStack);

            //  Expand the stack to take on all of the variable definitions
            //
            function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Expand the stack to encompass all variable definitions")});
            function_layout.instructions.push(code_gen::Instructions::MovValue{ source: required_function_size as u64, dest   : 0});
            function_layout.instructions.push(code_gen::Instructions::ExpandStack{ source: 0 });


            /*
            
                    Now that the stack is marked we get to calculate the size of the incoming parameters. To bring them into the stack frame we actually 
                    manually shift the frame marker lower on the stack to encompass the parameters in the form that they were left there by the caller
                    This allows us to not do a whole lot of work :)
        
            */

            if function.parameters.len() > 0 {

                let mut parameters_len : usize = 0;
                for parameter in function.parameters.iter() {
                    parameters_len += get_variable_data_size(&parameter.data_type);
                }
    
                // Get the frame pointer into register 2
                function_layout.instructions.push(code_gen::Instructions::Fp{ dest: 2 });

                // Move the inverse of the parameter length 
                function_layout.instructions.push(code_gen::Instructions::MovExact{ dest: 3, source: format!("{}", - (parameters_len as i64)) });

                // Add the negative value to the frame pointer - Add instructions will default to register 0 for the result
                function_layout.instructions.push(code_gen::Instructions::Add);

                // Update the frame pointer to wrap the parameters
                function_layout.instructions.push(code_gen::Instructions::Ufp{ source: 0 });

                //  Iterate over parameters adding them to the function layout storing the variable stack index
                //
                for parameter in function.parameters.iter() {

                    let variable_location = function_layout.stack_index.clone();
                    let variable_size = get_variable_data_size(&parameter.data_type);

                    function_layout.insert_variable(&parameter.name, variable_location);

                    //  Increase the stack index
                    //
                    function_layout.stack_index += variable_size;
                }
            }

            /*
            
                If there is a returnable piece of data coming from the function we need to ensure that we have some space for it. This means
                assigning some random and illegal variable name to keep track of it. 
            
            */

            let return_size = get_returnable_data_size(&function.return_type);
            if return_size > 0 {
                function_layout.insert_variable(&String::from(".__return__variable__"), function_layout.stack_index);
                
                function_layout.stack_index += return_size;
                function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Expand the stack to encompass return")});
                function_layout.instructions.push(code_gen::Instructions::MovValue{ source: return_size as u64, dest   : 0});
                function_layout.instructions.push(code_gen::Instructions::ExpandStack{ source: 0 });
            }

            /*
            
                Iterate over all of the statements in the function and handle them as needed.            
            
            */   
            if let Some(error) = build_statements(&mut function_layout, &function.statements) {
                return Err(error);
            }


            //  Now that the function has been built - we can extract its instruction set and add it 
            //  to the final instruction set
            //
            instruction_set.extend(function_layout.instructions);
        }
    }

    //for ins in instruction_set.iter() {
    //
    //    println!("{}", ins);
    //}

    vhigh!(v, vfunc!(), "Generating LVM code..");

    //  Optimize the generated instruction set, and then turn it into bytecode
    //
    let byte_code = code_gen::generate_bytecode(
        code_gen::optimize_instruction_set(instruction_set)
    );

    return Ok(byte_code);
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

fn build_statements(function_layout: &mut FunctionLayout, statements: &Vec<code_tree::Statements>) -> Option<GeneratorErrors> {

         

    for statement in statements.iter() {

        match &statement {

            /*
            
                Built In Function
            
            */
            code_tree::Statements::BuiltInFunction{ function, params } => {

                if let Some(e) = generate_built_in(function_layout, function, params) {
                    return Some(e);
                }
            }

            /*
            
                Function Call
            
            */
            code_tree::Statements::FunctionCall{ name, params } => {

                if let Some(e) = generate_function_call(function_layout, name, params) {
                    return Some(e);
                }
            }

            /*

                Variable Assignment
            
            */
            code_tree::Statements::Assignment{ variable, expression } => {

                if let Some(e) = generate_assignment(function_layout, variable, expression) {
                    return Some(e);
                }
            }

            /*

                Standard Variable Re-Assignment
            
            */
            code_tree::Statements::Reassignment{ variable, expression } => {

                if let Some(e) = generate_re_assignment(function_layout, variable, expression) {
                    return Some(e);
                }
            }

            /*

                Offset Variable Re-Assignment
            
            */
            code_tree::Statements::DotReassignment{ variable, offset, expression } => {
                
                if let Some(e) = generate_dot_re_assignment(function_layout, offset, variable, expression) {
                    return Some(e);
                }
            }

            /*

                Function Return
            
            */
            code_tree::Statements::Return{ expression } => {
                
                if let Some(e) = generate_return(function_layout, expression) {
                    return Some(e);
                }
            }

            /*

                For Loop
            
            */
            code_tree::Statements::ForLoop{ assignment, condition, re_assignment, instructions } => {

                if let Some(e) = generate_for_loop(function_layout, &**assignment, condition, &**re_assignment, instructions) {
                    return Some(e);
                }
            }

            /*

                While Loop
            
            */
            code_tree::Statements::WhileLoop{ condition, instructions } => {

                if let Some(e) = generate_while_loop(function_layout, condition, instructions) {
                    return Some(e);
                }
            }

            /*

                If Chain
            
            */

            code_tree::Statements::IfChain{ blocks } => {

                if let Some(e) = generate_if_chain(function_layout, blocks) {
                    return Some(e);
                }
            }
        }
    }

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_built_in(function_layout: &mut FunctionLayout, function: &code_tree::BuiltIn, params: &Vec<code_tree::Expression> ) -> Option<GeneratorErrors> {

    match function {

        /*
        
            Assert Equal

        */
        code_tree::BuiltIn::AssertEq => {

            // Lhs 
            if let Some(error) = build_expression(function_layout, &params[0]) { return Some(error); }
            let lhs_size = get_variable_data_size(&params[0].evaluated_type);

            // Rhs
            if let Some(error) = build_expression(function_layout, &params[1]) { return Some(error); }
            let rhs_size = get_variable_data_size(&params[1].evaluated_type);

            pop_from_stack(function_layout, lhs_size, 2);
            pop_from_stack(function_layout, rhs_size, 3);

            function_layout.instructions.push(code_gen::Instructions::AssertEq{ lhs: 2, rhs: 3});
        },

        /*
        
            Assert Not Equal

        */
        code_tree::BuiltIn::AssertNe => {

            // Lhs 
            if let Some(error) = build_expression(function_layout, &params[0]) { return Some(error); }
            let lhs_size = get_variable_data_size(&params[0].evaluated_type);

            // Rhs
            if let Some(error) = build_expression(function_layout, &params[1]) { return Some(error); }
            let rhs_size = get_variable_data_size(&params[1].evaluated_type);

            pop_from_stack(function_layout, lhs_size, 2);
            pop_from_stack(function_layout, rhs_size, 3);

            function_layout.instructions.push(code_gen::Instructions::AssertNe{ lhs: 2, rhs: 3});
        }
    }

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_function_call(function_layout: &mut FunctionLayout, name: &String, params: &Vec<code_tree::Expression> ) -> Option<GeneratorErrors> {

    //  Iterate over the parameter expressions and build them up
    //
    for parameter in params.iter() {
        if let Some(error) = build_expression(function_layout, &parameter) { return Some(error); }
    }

    // Call the function.
    //
    function_layout.instructions.push(code_gen::Instructions::Call{ name: name.clone() });

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_assignment(function_layout: &mut FunctionLayout, variable: &code_tree::Variable, expression: &code_tree::Expression ) -> Option<GeneratorErrors> {

    let variable_location = function_layout.stack_index.clone();
    let variable_size = get_variable_data_size(&variable.data_type);

    function_layout.insert_variable(&variable.name, variable_location);

    // Generate the instructions to calculate the expression
    //
    if let Some(error) = build_expression(function_layout, &expression) { return Some(error); }

    clone_to_stack(function_layout, variable_location.clone(), variable_size.clone());

    //  Increase the stack index
    //
    function_layout.stack_index += variable_size;

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_re_assignment(function_layout: &mut FunctionLayout, variable: &code_tree::Variable, expression: &code_tree::Expression ) -> Option<GeneratorErrors> {

    let variable_location = match function_layout.get_variable(&variable.name) {
        Some(loc) => loc.clone(),
        None => {
            return Some(GeneratorErrors::UndefinedReassignment{
                variable: variable.clone()
            });
        }
    };

    let variable_size = get_variable_data_size(&variable.data_type);

    // Generate the instructions to calculate the expression
    //
    if let Some(error) = build_expression(function_layout, &expression) { Some(error); }

    clone_to_stack(function_layout, variable_location.clone(), variable_size.clone());

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_dot_re_assignment(function_layout: &mut FunctionLayout, offset: &usize, variable: &code_tree::Variable, expression: &code_tree::Expression ) -> Option<GeneratorErrors> {

    let variable_location = match function_layout.get_variable(&variable.name) {
        Some(loc) => loc.clone() + offset,
        None => {
            return Some(GeneratorErrors::UndefinedReassignment{
                variable: variable.clone()
            });
        }
    };

    let variable_size = get_variable_data_size(&variable.data_type);

    // Generate the instructions to calculate the expression
    //
    if let Some(error) = build_expression(function_layout, &expression) { return Some(error); }

    clone_to_stack(function_layout, variable_location.clone(), variable_size.clone());
    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_return(function_layout: &mut FunctionLayout, expression: &Option<code_tree::Expression> ) -> Option<GeneratorErrors> {

    //  If an expression is present we need to calculate it, and then store
    //  the result to the front of the function
    //
    if let Some(expr) = expression {

        let variable_size = get_variable_data_size(&expr.evaluated_type);

        // Generate the instructions to calculate the expression
        //
        if let Some(error) = build_expression(function_layout, &expr) { return Some(error); }


        clone_to_stack(function_layout, 0, variable_size.clone());

        // Return value should now be in index 0 relative to the frame pointer, so now we have to move the frame pointer past 
        // the return value before clearing the stack
        function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Move the frame pointer to exclude return value for stack reset")});
        function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("")});

        function_layout.instructions.push(code_gen::Instructions::Fp{ dest: 2 });
        function_layout.instructions.push(code_gen::Instructions::MovExact{ dest: 3, source: format!("{}", variable_size) });
        function_layout.instructions.push(code_gen::Instructions::Add);   // Places location in r0

        function_layout.instructions.push(code_gen::Instructions::Ufp{ source: 0 });

    } 

    //  Clear the stack up-to the last mark - If a return value existed
    //  the mark should take place up-to but not including that data
    //
    function_layout.instructions.push(code_gen::Instructions::ResetStack);

    //  Return
    //
    function_layout.instructions.push(code_gen::Instructions::Return);

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_for_loop(function_layout: &mut FunctionLayout, assignment: &code_tree::Statements, condition: &code_tree::Expression, re_assignment: &code_tree::Statements, instructions: &Vec<code_tree::Statements>) -> Option<GeneratorErrors> {

    //  Create a new context for the for loop
    //
    function_layout.new_variable_context();

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Starting for loop")});

    let mut statements : Vec<code_tree::Statements> = Vec::new();

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("For loop assignment")});

    //  Build the assignment
    //
    statements.push(assignment.clone());
    if let Some(error) = build_statements(function_layout, &statements) {
        return Some(error);
    }

    //  Get the label that will allow us to jump to the end of the for loop
    //
    let for_loop_top_label = function_layout.get_label();
    let for_loop_bottom_label = function_layout.get_label();

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("For loop top label")});

    //  Write the top label
    //
    function_layout.instructions.push(code_gen::Instructions::Label{ name: for_loop_top_label.clone() });

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Build conditional statement")});

    //  Build the expression
    //
    if let Some(error) = build_expression(function_layout, condition) {
        return Some(error);
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Get value of conditional calculation")});

    //  Pop the result of the expression into r2
    //
    pop_from_stack(function_layout, get_variable_data_size(&condition.evaluated_type), 2);

    //  If the condition is less than or equal to 0 we don't want to loop again, so we will then goto
    //  the bottom label
    //
    function_layout.instructions.push(code_gen::Instructions::MovValue{ dest: 3, source: 0 });

    //  Check the condition, See if we need to jump to the bottom label
    //
    match condition.evaluated_type {

        //  If its a float, we need to do the float branch
        //
        code_tree::VarTypes::Float => {

            function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Check float value for branch")});

            function_layout.instructions.push(code_gen::Instructions::LteF{ label: for_loop_bottom_label.clone() });
        }

        //  Everything else can be handled by the standard integer branch
        //
        _ => {

            function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Check integer value for branch")});

            function_layout.instructions.push(code_gen::Instructions::Lte{ label: for_loop_bottom_label.clone() });
        }
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("For loop statements")});

    //  Iterate over statements and build them
    //
    if let Some(error) = build_statements(function_layout, &instructions) {
        return Some(error);
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("For loop driving variable re assignment")});

    //  Build the re assignment to increment the control variable
    //
    statements.clear();
    statements.push(re_assignment.clone());

    if let Some(error) = build_statements(function_layout, &statements) {
        return Some(error);
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Jump to the top of the loop")});

    //  Goto the top label to perform the check again
    //
    function_layout.instructions.push(code_gen::Instructions::Goto{ label: for_loop_top_label.clone() });


    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("For loop bottom label")});

    //  Write the bottom label
    //
    function_layout.instructions.push(code_gen::Instructions::Label{ name : for_loop_bottom_label.clone() });

    // Remove the for loop's context
    //
    function_layout.pop_context();

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_while_loop(function_layout: &mut FunctionLayout, condition: &code_tree::Expression, instructions: &Vec<code_tree::Statements>) -> Option<GeneratorErrors> {

    //  Create a new context for the for loop
    //
    function_layout.new_variable_context();

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Starting while loop")});

    //  Get the label that will allow us to jump to the end of the while loop
    //
    let while_loop_top_label = function_layout.get_label();
    let while_loop_bottom_label = function_layout.get_label();

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("While loop top label")});

    //  Write the top label
    //
    function_layout.instructions.push(code_gen::Instructions::Label{ name: while_loop_top_label.clone() });

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Build conditional statement")});

    //  Build the expression
    //
    if let Some(error) = build_expression(function_layout, condition) {
        return Some(error);
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Get value of conditional calculation")});

    //  Pop the result of the expression into r2
    //
    pop_from_stack(function_layout, get_variable_data_size(&condition.evaluated_type), 2);

    //  If the condition is less than or equal to 0 we don't want to loop again, so we will then goto
    //  the bottom label
    //
    function_layout.instructions.push(code_gen::Instructions::MovValue{ dest: 3, source: 0 });

    //  Check the condition, See if we need to jump to the bottom label
    //
    match condition.evaluated_type {

        //  If its a float, we need to do the float branch
        //
        code_tree::VarTypes::Float => {

            function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Check float value for branch")});

            function_layout.instructions.push(code_gen::Instructions::LteF{ label: while_loop_bottom_label.clone() });
        }

        //  Everything else can be handled by the standard integer branch
        //
        _ => {

            function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Check integer value for branch")});

            function_layout.instructions.push(code_gen::Instructions::Lte{ label: while_loop_bottom_label.clone() });
        }
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("While loop statements")});

    //  Iterate over statements and build them
    //
    if let Some(error) = build_statements(function_layout, &instructions) {
        return Some(error);
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Jump to the top of the loop")});

    //  Goto the top label to perform the check again
    //
    function_layout.instructions.push(code_gen::Instructions::Goto{ label: while_loop_top_label.clone() });


    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("While loop bottom label")});

    //  Write the bottom label
    //
    function_layout.instructions.push(code_gen::Instructions::Label{ name : while_loop_bottom_label.clone() });

    // Remove the for loop's context
    //
    function_layout.pop_context();

    None
}
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn generate_if_chain(function_layout: &mut FunctionLayout, blocks: &Vec<code_tree::IfBlock>) -> Option<GeneratorErrors> {

    // Label, Condition, Instructions
    //
    // We will need this so we can build the actual code blocks after doing the branch building
    //
    let mut block_data : Vec<( String, &Vec<code_tree::Statements> )> = Vec::new();

    //  For the bottom of the statemnts
    //
    let end_statement_label = function_layout.get_label();

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Check conditions for if-statement block(s)")});
    for block in blocks.iter() {

        let current_block_label = function_layout.get_label();

        block_data.push(
            (
                current_block_label.clone(),
                &block.instructions
            )
        );

        //  Build the condition
        //
        if let Some(error) = build_expression(function_layout, &block.condition) {
            return Some(error);
        }

        //  Get the result into r2
        //
        pop_from_stack(function_layout, get_variable_data_size(&block.condition.evaluated_type), 2);    

        function_layout.instructions.push(code_gen::Instructions::MovValue{ dest: 3, source: 0});

        //  Jump to the condition if it evaluates to being  > 0
        //
        match block.condition.evaluated_type {
            code_tree::VarTypes::Float => { function_layout.instructions.push(code_gen::Instructions::GtF{ label: current_block_label.clone() }); }
            _ =>                          { function_layout.instructions.push(code_gen::Instructions::Gt { label: current_block_label.clone() }); }
        }
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("Goto end of statements if none triggered")});

    //  Ensure that if none of the things are true we skip everything
    //
    function_layout.instructions.push(code_gen::Instructions::Goto{ label: end_statement_label.clone() });


    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("If-statement instruction segments")});

    //  Now that  
    //
    for (label, statement_vec) in block_data.iter() {

        function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("If-statement block")});
        function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("")});

        //  Place the section label
        //
        function_layout.instructions.push(code_gen::Instructions::Label{ name: label.clone() });

        //  Place all of its instructions
        //
        if let Some(error) = build_statements(function_layout, &statement_vec) {
            return Some(error);
        }

        // Go to end statement when completed
        //
        function_layout.instructions.push(code_gen::Instructions::Goto{ label: end_statement_label.clone() });
    }

    function_layout.instructions.push(code_gen::Instructions::Comment{ data: String::from("End if-statement block(s)")});

    //  Finalize everything with the end label to ensure that we can skip everything if we need to
    //
    function_layout.instructions.push(code_gen::Instructions::Label{ name: end_statement_label });

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn get_variable_data_size(assigned_type: &code_tree::VarTypes ) -> usize {

    match &*assigned_type {

        code_tree::VarTypes::Integer => {  data_settings::INTEGER_SIZE  },
        code_tree::VarTypes::Float   => {  data_settings::FLOAT_SIZE    },
        code_tree::VarTypes::Char    => {  data_settings::CHAR_SIZE     },
        code_tree::VarTypes::Nil     => {  data_settings::NIL_SIZE      },
        code_tree::VarTypes::Struct{ definition } => {

            definition.size
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn get_returnable_data_size(assigned_type: &code_tree::VarTypes ) -> usize {

    match &*assigned_type {

        code_tree::VarTypes::Integer => { data_settings::INTEGER_SIZE },
        code_tree::VarTypes::Float   => { data_settings::FLOAT_SIZE   },
        code_tree::VarTypes::Char    => { data_settings::CHAR_SIZE    },
        code_tree::VarTypes::Nil     => { data_settings::NIL_SIZE     },
        code_tree::VarTypes::Struct{ definition } => {
            definition.size
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn clone_to_stack(func: &mut FunctionLayout, location: usize, size: usize) { 

    //  Determine if we can move the chunk of memory by 8/4/ or 1. This is to ensure we use as few instructions 
    //  as possible to move the thing
    //
    let increment : u64;
    let load_instruction: code_gen::Instructions;
    let store_instruction: code_gen::Instructions;

    if size % 8 == 0 {
        increment = 8;
        load_instruction = code_gen::Instructions::LoadStackWord{ source_addr: 13, dest: 0 };
        store_instruction = code_gen::Instructions::StoreStackWord{ source: 0, dest_addr: 15 };
    } else if size % 4 == 0 {
        increment = 4;
        load_instruction = code_gen::Instructions::LoadStackHalf{ source_addr: 13, dest: 0 };
        store_instruction = code_gen::Instructions::StoreStackHalf{ source: 0, dest_addr: 15 };
    } else {
        increment = 1;
        load_instruction = code_gen::Instructions::LoadStackByte{ source_addr: 13, dest: 0 };
        store_instruction = code_gen::Instructions::StoreStackByte{ source: 0, dest_addr: 15 };
    }

    // Comment
    func.instructions.push(code_gen::Instructions::Comment{ data: String::from("Clone item into stack from the end")});

    // Get item for additions in scanning
    func.instructions.push(code_gen::Instructions::MovValue { dest: 14, source: increment });

    // Get the current size of the stack and save it off to a reg
    func.instructions.push(code_gen::Instructions::StackSize{ dest: 15 });

    // Calculate the beginning of the thing we need to grab
    func.instructions.push(code_gen::Instructions::MovExact { dest: 2, source: format!("-{}", size) });
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 13, lhs: 2, rhs: 15});


    // calculate the start position of the item we need to grab
    func.instructions.push(code_gen::Instructions::MovValue{ dest: 2, source: location as u64});
    func.instructions.push(code_gen::Instructions::Fp      { dest: 3 });
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 15, lhs: 2, rhs: 3});   // Places location in r13

    // Counter
    func.instructions.push(code_gen::Instructions::MovValue{ dest: 2, source: 0});

    // Copy the size of the object to 3
    func.instructions.push(code_gen::Instructions::MovValue{ source: size as u64, dest: 3});

    //  r2  - Counter
    //  r3  - Size of the object
    //  r13 - Starting point of the item
    //  r14 - '1' Used for adding to the scanner
    //  r15 - Where the thing will be going

    let loop_top = func.get_label();

    func.instructions.push(code_gen::Instructions::Label{ name: loop_top.clone() });

    // Move a byte of the object to the new area
    func.instructions.push(load_instruction);
    func.instructions.push(store_instruction);

    // Increase the area that we are reading from and the area that we are storing to

    // Add To the starting point
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 13, lhs: 13, rhs: 14}); 

    // Add to the counter
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 2, lhs: 2, rhs: 14}); 

    // Add to the destination
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 15, lhs: 15, rhs: 14}); 

    // Compare counter (2) to size of the object being moved (3)
    func.instructions.push(code_gen::Instructions::Lt{  label: loop_top.clone() });
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn clone_from_stack(func: &mut FunctionLayout, location: usize, size: usize) { 

    //  Determine if we can move the chunk of memory by 8/4/ or 1. This is to ensure we use as few instructions 
    //  as possible to move the thing
    //
    let increment : u64;
    let load_instruction: code_gen::Instructions;
    let store_instruction: code_gen::Instructions;

    if size % 8 == 0 {
        increment = 8;
        load_instruction = code_gen::Instructions::LoadStackWord{ source_addr: 13, dest: 0 };
        store_instruction = code_gen::Instructions::StoreStackWord{ source: 0, dest_addr: 15 };
    } else if size % 4 == 0 {
        increment = 4;
        load_instruction = code_gen::Instructions::LoadStackHalf{ source_addr: 13, dest: 0 };
        store_instruction = code_gen::Instructions::StoreStackHalf{ source: 0, dest_addr: 15 };
    } else {
        increment = 1;
        load_instruction = code_gen::Instructions::LoadStackByte{ source_addr: 13, dest: 0 };
        store_instruction = code_gen::Instructions::StoreStackByte{ source: 0, dest_addr: 15 };
    }

    // Comment
    func.instructions.push(code_gen::Instructions::Comment{ data: String::from("Clone item onto end of the stack")});

    // Get item for additions in scanning
    func.instructions.push(code_gen::Instructions::MovValue { dest: 14, source: increment });

    // Get the current size of the stack and save it off to a reg
    func.instructions.push(code_gen::Instructions::StackSize{ dest: 15 });

    // Expand the stack so we know we have the room to place it
    func.instructions.push(code_gen::Instructions::MovValue{ source: size as u64, dest   : 0});
    func.instructions.push(code_gen::Instructions::ExpandStack{ source: 0 });

    // calculate the start position of the item we need to grab
    func.instructions.push(code_gen::Instructions::MovValue{ dest: 2, source: location as u64});
    func.instructions.push(code_gen::Instructions::Fp      { dest: 3 });
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 13, lhs: 2, rhs: 3});   // Places location in r13

    // Counter
    func.instructions.push(code_gen::Instructions::MovValue{ dest: 2, source: 0});

    // Copy the size of the object to 3
    func.instructions.push(code_gen::Instructions::MovValue{ source: size as u64, dest: 3});

    //  r2  - Counter
    //  r3  - Size of the object
    //  r13 - Starting point of the item
    //  r14 - '1' Used for adding to the scanner
    //  r15 - The stack size before expansion (where the new item starts going)

    let loop_top = func.get_label();

    func.instructions.push(code_gen::Instructions::Label{ name: loop_top.clone() });

    // Move a byte of the object to the new area
    func.instructions.push(load_instruction);
    func.instructions.push(store_instruction);

    // Increase the area that we are reading from and the area that we are storing to

    // Add To the starting point
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 13, lhs: 13, rhs: 14}); 

    // Add to the counter
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 2, lhs: 2, rhs: 14}); 

    // Add to the destination
    func.instructions.push(code_gen::Instructions::AddSpecific{ dest: 15, lhs: 15, rhs: 14}); 

    // Compare counter (2) to size of the object being moved (3)
    func.instructions.push(code_gen::Instructions::Lt{  label: loop_top.clone() });
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn pop_from_stack(func: &mut FunctionLayout, size: usize, dest: u8) {

    match size {

        data_settings::WORD_SIZE => { func.instructions.push(code_gen::Instructions::PopWord{ dest }); }
        data_settings::HALF_SIZE => { func.instructions.push(code_gen::Instructions::PopHalf{ dest }); }
        data_settings::BYTE_SIZE => { func.instructions.push(code_gen::Instructions::PopByte{ dest }); }

        _ => {
            panic!("<POP> Item size in block not handled. This could happen if you are expanding the language. Size : {}", size);
        }
    }

}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

#[inline(always)]
fn push_to_stack(func: &mut FunctionLayout, size: usize, source: u8) {

    match size {

        data_settings::WORD_SIZE => { func.instructions.push(code_gen::Instructions::PushWord{ source }); }
        data_settings::HALF_SIZE => { func.instructions.push(code_gen::Instructions::PushHalf{ source }); }
        data_settings::BYTE_SIZE => { func.instructions.push(code_gen::Instructions::PushByte{ source }); }

        _ => {
            panic!("<PUSH> Item size in block not handled. This could happen if you are expanding the language. Size : {}", size);
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

// Simple Conditional operators ( > >= == <= < )
macro_rules! conditional_build {
    ($float_ins:expr, $integer_ins:expr, $func:expr, $expression:expr, $bl:expr, $blb:expr) => {
        
        pop_from_stack($func, get_variable_data_size(&$expression.evaluated_type), 3);
        pop_from_stack($func, get_variable_data_size(&$expression.evaluated_type), 2);

        $func.instructions.push(code_gen::Instructions::MovValue{ dest: 0, source: 1});

        match $expression.evaluated_type {

            code_tree::VarTypes::Float => {
                $func.instructions.push($float_ins);
            }

            _ => {
                $func.instructions.push($integer_ins);
            }
        }
        // If we fall through the branch, we mark 0 and goto the bottom of the condition
        $func.instructions.push(code_gen::Instructions::MovValue{ dest: 0, source: 0});
        push_to_stack($func, get_variable_data_size(&$expression.evaluated_type), 0);
        
        $func.instructions.push(code_gen::Instructions::Goto{ label: $blb.clone()});

        // If we don't we load the one that was loaded above for comparison
        $func.instructions.push(code_gen::Instructions::Label{ name: $bl.clone() });
        push_to_stack($func, get_variable_data_size(&$expression.evaluated_type), 0);
        $func.instructions.push(code_gen::Instructions::Label{ name: $blb.clone() });
    };
}

// Add, Sub, Div, Mul, and their float variants
macro_rules! poly_variant_arithmetic {
    ($float_ins:expr, $integer_ins:expr, $func:expr, $expression:expr) => {
        pop_from_stack($func, get_variable_data_size(&$expression.evaluated_type), 3);
        pop_from_stack($func, get_variable_data_size(&$expression.evaluated_type), 2);
        if $expression.evaluated_type == code_tree::VarTypes::Float {
            $func.instructions.push($float_ins);    
        } else {
            $func.instructions.push($integer_ins); 
        }
        push_to_stack($func, get_variable_data_size(&$expression.evaluated_type), 0);
    };
}

// pow, lsh, rsh, mod bwor bwand xor
macro_rules! mono_variant_arithmetic {
    ($ins:expr, $func:expr, $expression:expr) => {
        pop_from_stack($func, get_variable_data_size(&$expression.evaluated_type), 3);
        pop_from_stack($func, get_variable_data_size(&$expression.evaluated_type), 2);
        $func.instructions.push($ins); 
        push_to_stack($func, get_variable_data_size(&$expression.evaluated_type), 0);
    };
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------


/*
    Note about the quality of instructions generated: 

        The instructions generated here are done so to work 'generally' that meaning they aren't going
        to be optimized. For some things like math there are loads / push / pop / operate instructions 
        generated because the block doesn't know what the hell is going on from the builder, and the builder
        also doesn't have too much context as the level above it also doesn't have enough context to say how many
        items will be loaded / operated on. These are being generated as they are being scanned so that makes 
        for bad assembly instructions.. or at least.. not optimized ones. The step after this would be to
        scan the list of instructions and see where optimizations can come in.

    Notes about register usage: 

        Register 2 will hold the left hand side of any given mathematical operation, and 3 will hold the right hand size

        Register 0 will be used to hold words that are are to be pushed onto the stack
*/

fn build_expression(func: &mut FunctionLayout, expression: &code_tree::Expression ) -> Option<GeneratorErrors> {

    for expr_node in expression.definition.iter() {

        match expr_node {

            /*
                    Load an Integer
                    Move the integer to a register and then make a push instruction to put it on the stack
            */
            code_tree::ExpressionNode::RawInteger{ value } =>{

                func.instructions.push(code_gen::Instructions::MovExact{ source: format!("{}", value), dest   : 0});
                push_to_stack(func, get_variable_data_size(&code_tree::VarTypes::Integer), 0);
            }

            /*
                    Load a Float
                    Move the float to a register and then make a push instruction to put it on the stack
            */
            code_tree::ExpressionNode::RawFloat{ value }   =>{

                let bytes = value.to_le_bytes();
                let value = u64::from_le_bytes(bytes);
                func.instructions.push(code_gen::Instructions::MovValue{ source: value, dest   : 0});
                push_to_stack(func, get_variable_data_size(&code_tree::VarTypes::Float), 0);
            }

            /*
                    Load a Char
                    Move the char to a register and then make a push instruction to put it on the stack
            */
            code_tree::ExpressionNode::RawChar{ value}  =>{

                let half_word = value.chars().next().unwrap();
                func.instructions.push(code_gen::Instructions::MovValue{ source: half_word as u64, dest: 0});
                push_to_stack(func, get_variable_data_size(&&code_tree::VarTypes::Char), 0);
            }

            /*
                    Load a variable
                    Copy the stored location of the variable to a register, and then push that item onto the end of the
                    stack to take part in the calculation of the expression
            */
            code_tree::ExpressionNode::Variable{ value }  =>{


                func.instructions.push(
                    code_gen::Instructions::Comment{ data: format!("Load Variable : {} ", value.name.clone()) }
                );
                func.instructions.push(
                    code_gen::Instructions::Comment{ data: String::from("") }
                );

                let variable_location = match func.get_variable(&value.name) {
                    Some(v) => v.clone(),
                    None => {
                        return Some(GeneratorErrors::UndefinedReassignment{
                            variable: value.clone()
                        });
                    }
                };

                let size = get_variable_data_size(&value.data_type);

                clone_from_stack(func, variable_location.clone(), size);
            }

            /*
                    Load a variable that is being accessed via an operator
                    Copy the stored location of the variable+offset to a register, and then push that item onto the end of the
                    stack to take part in the calculation of the expression
            */
            code_tree::ExpressionNode::Accessor{ value,  offset }  =>{

                let variable_location = match func.get_variable(&value.name) {
                    Some(v) => v.clone() + offset,
                    None => {
                        return Some(GeneratorErrors::UndefinedReassignment{
                            variable: value.clone()
                        });
                    }
                };

                func.instructions.push(code_gen::Instructions::MovValue{ dest: 0, source: variable_location as u64});

                let size = get_variable_data_size(&value.data_type);

                clone_from_stack(func, variable_location.clone(), size);
            }

            /*
                Artifact from semantic scanning
            */
            code_tree::ExpressionNode::Structure{ definition: _ }   =>{

            }

            /*
                Push each param to the stack. They should be reversed relative to the
                order of declaration by the user by the scanner. 
            
            */
            code_tree::ExpressionNode::FunctionCall{ name, params, return_type: _}  =>{

                func.instructions.push(
                    code_gen::Instructions::Comment{
                        data: 
                        format!("Loading {} parameters for call to {}", params.len(), name)
                    }
                );


                //  Iterate over the parameter expressions and build them up
                //
                for parameter in params.iter() {


                    func.instructions.push(
                        code_gen::Instructions::Comment{
                            data: 
                            format!("Loading parameter expression: {:?}", parameter)
                        }
                    );

                    if let Some(error) = build_expression(func, &*parameter) { return Some(error); }
                }

                // Call the function. If it returns its item should be on the stack
                //
                func.instructions.push(code_gen::Instructions::Call{ name: name.clone() });
            }

            /*

                Arithmetic Instructions

            */
            code_tree::ExpressionNode::Mul  =>{
                poly_variant_arithmetic!(code_gen::Instructions::MulF, code_gen::Instructions::Mul, func, expression);
            }
            code_tree::ExpressionNode::Div  =>{

                poly_variant_arithmetic!(code_gen::Instructions::DivF, code_gen::Instructions::Div, func, expression);
            }
            code_tree::ExpressionNode::Add  =>{
                poly_variant_arithmetic!(code_gen::Instructions::AddF, code_gen::Instructions::Add, func, expression);
            }
            code_tree::ExpressionNode::Sub  =>{
                poly_variant_arithmetic!(code_gen::Instructions::SubF, code_gen::Instructions::Sub, func, expression);
            }
            code_tree::ExpressionNode::Pow  =>{
                mono_variant_arithmetic!(code_gen::Instructions::Pow, func, expression);
            }
            code_tree::ExpressionNode::Mod  =>{
                mono_variant_arithmetic!(code_gen::Instructions::Mod, func, expression);
            }
            code_tree::ExpressionNode::Lsh  =>{
                mono_variant_arithmetic!(code_gen::Instructions::Lsh, func, expression);
            }
            code_tree::ExpressionNode::Rsh  =>{
                mono_variant_arithmetic!(code_gen::Instructions::Rsh, func, expression);
            }
            code_tree::ExpressionNode::BwXor    =>{
                mono_variant_arithmetic!(code_gen::Instructions::Xor, func, expression);
            }
            code_tree::ExpressionNode::BwOr =>{
                mono_variant_arithmetic!(code_gen::Instructions::BwOr, func, expression);
            }
            code_tree::ExpressionNode::BwAnd    =>{
                mono_variant_arithmetic!(code_gen::Instructions::BwAnd, func, expression);
            }

            // This is the only mono_variant unary so it is constructed in-place
            code_tree::ExpressionNode::BwNot =>{
                pop_from_stack(func, get_variable_data_size(&expression.evaluated_type), 2);
                func.instructions.push(code_gen::Instructions::Not); 
                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 0);
            }

            /*
            
                Less than or Equal to
            
            */
            code_tree::ExpressionNode::Lte  =>{
                let branch_label = func.get_label();
                let branch_label_bottom = func.get_label();
                conditional_build!(
                    code_gen::Instructions::LteF{ label: branch_label.clone()},
                    code_gen::Instructions::Lte { label: branch_label.clone()},
                    func,
                    expression,
                    branch_label,
                    branch_label_bottom);
            }

            /*
            
                Greater than or Equal to
            
            */
            code_tree::ExpressionNode::Gte  =>{
                let branch_label = func.get_label();
                let branch_label_bottom = func.get_label();
                conditional_build!(
                    code_gen::Instructions::GteF{ label: branch_label.clone()},
                    code_gen::Instructions::Gte { label: branch_label.clone()},
                    func,
                    expression,
                    branch_label,
                    branch_label_bottom);
            }

            /*
            
                Greater Than
            
            */
            code_tree::ExpressionNode::Gt   =>{
                let branch_label = func.get_label();
                let branch_label_bottom = func.get_label();
                conditional_build!(
                    code_gen::Instructions::GtF{ label: branch_label.clone()},
                    code_gen::Instructions::Gt { label: branch_label.clone()},
                    func,
                    expression,
                    branch_label,
                    branch_label_bottom);
            }

            /*
            
                Less Than
            
            */
            code_tree::ExpressionNode::Lt   =>{
                let branch_label = func.get_label();
                let branch_label_bottom = func.get_label();
                conditional_build!(
                    code_gen::Instructions::LtF{ label: branch_label.clone()},
                    code_gen::Instructions::Lt { label: branch_label.clone()},
                    func,
                    expression,
                    branch_label,
                    branch_label_bottom);
            }

            /*
            
                Equal to
            
            */
            code_tree::ExpressionNode::Equal    =>{
                let branch_label = func.get_label();
                let branch_label_bottom = func.get_label();
                conditional_build!(
                    code_gen::Instructions::EqF{ label: branch_label.clone()},
                    code_gen::Instructions::Eq { label: branch_label.clone()},
                    func,
                    expression,
                    branch_label,
                    branch_label_bottom);
            }
            
            /*
            
               Not Equal to
            
            */
            code_tree::ExpressionNode::Ne   =>{
                let branch_label = func.get_label();
                let branch_label_bottom = func.get_label();
                conditional_build!(
                    code_gen::Instructions::NeF{ label: branch_label.clone()},
                    code_gen::Instructions::Ne { label: branch_label.clone()},
                    func,
                    expression,
                    branch_label,
                    branch_label_bottom);
            }

            /*
            
                OR
            
            */
            code_tree::ExpressionNode::Or   =>{

                pop_from_stack(func, get_variable_data_size(&expression.evaluated_type), 8);
                pop_from_stack(func, get_variable_data_size(&expression.evaluated_type), 2);

                func.instructions.push(code_gen::Instructions::MovValue    { dest: 3, source: 0});

                let success  = func.get_label();
                let complete = func.get_label();

                match expression.evaluated_type {
                    code_tree::VarTypes::Float => {
                        func.instructions.push(code_gen::Instructions::GtF{ label: success.clone() });
                    }
                    _ => {
                        func.instructions.push(code_gen::Instructions::Gt{ label: success.clone() });
                    }
                }
                
                func.instructions.push(code_gen::Instructions::MovRegister{ dest: 2, source: 8});

                match expression.evaluated_type {
                    code_tree::VarTypes::Float => {
                        func.instructions.push(code_gen::Instructions::GtF{ label: success.clone() });
                    }
                    _ => {
                        func.instructions.push(code_gen::Instructions::Gt{ label: success.clone() });
                    }
                }

                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 3);
                func.instructions.push(code_gen::Instructions::Goto{ label: complete.clone()});

                func.instructions.push(code_gen::Instructions::Label{ name: success});
                func.instructions.push(code_gen::Instructions::MovValue{ dest:   0, source: 1});
                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 0);

                func.instructions.push(code_gen::Instructions::Label{ name: complete});
            }

            /*
            
                AND
            
            */
            code_tree::ExpressionNode::And  =>{

                pop_from_stack(func, get_variable_data_size(&expression.evaluated_type), 8);
                pop_from_stack(func, get_variable_data_size(&expression.evaluated_type), 2);

                func.instructions.push(code_gen::Instructions::MovValue    { dest: 3, source: 0});

                let fail  = func.get_label();
                let complete = func.get_label();

                match expression.evaluated_type {
                    code_tree::VarTypes::Float => {
                        func.instructions.push(code_gen::Instructions::LteF{ label: fail.clone() });
                    }
                    _ => {
                        func.instructions.push(code_gen::Instructions::Lte { label: fail.clone() });
                    }
                }
                
                func.instructions.push(code_gen::Instructions::MovRegister{ dest: 2, source: 8});

                match expression.evaluated_type {
                    code_tree::VarTypes::Float => {
                        func.instructions.push(code_gen::Instructions::LteF{ label: fail.clone() });
                    }
                    _ => {
                        func.instructions.push(code_gen::Instructions::Lte { label: fail.clone() });
                    }
                }

                func.instructions.push(code_gen::Instructions::MovValue{ dest:   0, source: 1});
                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 0);

                func.instructions.push(code_gen::Instructions::Goto{ label: complete.clone()});

                func.instructions.push(code_gen::Instructions::Label{ name: fail});

                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 3);

                func.instructions.push(code_gen::Instructions::Label{ name: complete});
            }

            /*
            
                NEGATE
            
            */
            code_tree::ExpressionNode::Negate   =>{

                pop_from_stack(func, get_variable_data_size(&expression.evaluated_type), 2);

                func.instructions.push(code_gen::Instructions::MovValue    { dest: 3, source: 1});

                let place_zero  = func.get_label();
                let complete = func.get_label();

                match expression.evaluated_type {
                    code_tree::VarTypes::Float => {
                        func.instructions.push(code_gen::Instructions::GteF{ label: place_zero.clone() });
                    }
                    _ => {
                        func.instructions.push(code_gen::Instructions::Gte { label: place_zero.clone() });
                    }
                }
                
                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 3);
                func.instructions.push(code_gen::Instructions::Goto{ label: complete.clone()});


                func.instructions.push(code_gen::Instructions::Label{ name: place_zero});
       
                func.instructions.push(code_gen::Instructions::MovValue { dest: 0, source: 0});
                push_to_stack(func, get_variable_data_size(&expression.evaluated_type), 0);

                func.instructions.push(code_gen::Instructions::Label{ name: complete});
            }

        }
    }

    None
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------


#[inline(always)]
fn calculate_function_size(function: &code_tree::Function) -> usize {

    let mut required_stack_size : usize = 0;

    for parameter in function.parameters.iter() {

        required_stack_size += get_variable_data_size(&parameter.data_type);
    }

    return required_stack_size + calculate_instruction_stack_space(&function.statements);
}

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

fn calculate_instruction_stack_space(statements: &Vec<code_tree::Statements>) -> usize {

    let mut required_stack_size : usize = 0;

    for statement in statements.iter() {

        match &statement {
            code_tree::Statements::Assignment{ variable, expression: _ } => {

                required_stack_size += get_variable_data_size(&variable.data_type);
            }

            // If its a for loop we will need to dig out the assignments as well for the loopy part
            code_tree::Statements::ForLoop{ assignment, condition: _, re_assignment: _, instructions} => {

                match &**assignment {
                    code_tree::Statements::Assignment{ variable, expression: _} => {

                        required_stack_size += get_variable_data_size(&variable.data_type);
                    }
                    _ => {
                    }
                }

                required_stack_size += calculate_instruction_stack_space(&instructions);
            }

            // If its a while loop we will need to dig out the assignments
            code_tree::Statements::WhileLoop{ condition: _, instructions} => {

                required_stack_size += calculate_instruction_stack_space(&instructions);
            }

            // If its an if-chain we need to dig out assignments too. In this case though
            // since only one block will ever be triggered, but we can't tell which one it will
            // be what we do is find the block with the MOST assignments and use that as the marker.
            code_tree::Statements::IfChain{ blocks } => {

                let mut largest_assignment : usize = 0;
                for block in blocks.iter() {
                    let amnt = calculate_instruction_stack_space(&block.instructions);
                    if amnt > largest_assignment {
                        largest_assignment = amnt;
                    }
                }
                required_stack_size += largest_assignment;
            }

            _ =>  {}
        }
    }
    return required_stack_size;
}