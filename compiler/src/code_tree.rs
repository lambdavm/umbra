/*
    About:

        Code_tree is the last formation of the code in a tree structure and is used to generate the pseudo assembly enum instructions
        found in the code_gen crate. This structure is generated from a parse_tree by the code_scanner. The code_scanner aims to ensure
        that this code_tree is 100% syntactically valid.

*/

use crate::parse_tree;
use parse_tree::Location;

use derive_more::{ Display };

#[derive(Debug, Clone)]
pub struct CodeTree {
    pub structures: Vec<Structure>,
    pub functions:  Vec<Function>,
    pub file:       String
}

impl CodeTree {
    pub fn new(file: &String) -> Self {
        Self {
            structures: Vec::new(),
            functions:  Vec::new(),
            file:       file.clone()
        }
    }
}

#[derive(Display, Debug, Clone)]
#[display(fmt = "Variable <{}>", name)]
pub struct Variable {
    pub name: String,
    pub data_type: VarTypes,
    pub location: Location
}

#[derive(Debug, Display, Clone, std::cmp::PartialEq)]
pub enum VarTypes {
    Integer,
    Float,
    Char,
    #[display(fmt = "{}", definition)]
    Struct{ definition: Structure },
    Nil
}

#[derive(Debug, Clone)]
pub enum ExpressionNode {
    RawInteger{ value: i64 },
    RawFloat{ value: f64 },
    RawChar{ value: String},
    Variable{ value: Variable },
    FunctionCall{ name: String, params: Vec<Expression>, return_type: VarTypes},
    Accessor{ value: Variable,  offset: usize },
    Structure{ definition: Structure },
    Mul,
    Div,
    Add,
    Sub,
    Lte,
    Gte,
    Gt,
    Lt,
    Equal,
    Ne,
    Pow,
    Mod,
    Lsh,
    Rsh,
    BwXor,
    BwOr,
    BwAnd,
    Or,
    And,
    Negate,
    BwNot
}

#[derive(Debug, Clone)]
pub struct Expression {

    pub location: Location,
    pub evaluated_type: VarTypes,
    pub definition: Vec<ExpressionNode>,
}

#[derive(Debug, Display, Clone, std::cmp::PartialEq)]
#[display(fmt = "Struct<{}>", name)]
pub struct Structure {
    pub name: String,
    pub members: Vec<VarTypes>,
    pub location: Location,
    pub size: usize
}

#[derive(Debug, Clone)]
pub struct Function {
    pub name: String,
    pub return_type: VarTypes,
    pub statements: Vec<Statements>,
    pub parameters: Vec<Variable>,
    pub location: Location
}

#[derive(Debug, Clone)]
pub enum BuiltIn {
    AssertEq,
    AssertNe
}

#[derive(Debug, Clone)]
pub struct IfBlock {
    pub condition:  Expression,
    pub instructions: Vec<Statements>,
}

#[derive(Debug, Clone)]
pub enum Statements {

    BuiltInFunction {
        function: BuiltIn,
        params: Vec<Expression>
    },

    FunctionCall {
        name: String, 
        params: Vec<Expression>
    },

    Assignment {
        variable: Variable,
        expression: Expression
    },

    Reassignment { 
        variable: Variable,
        expression: Expression
    },

    DotReassignment { 
        variable: Variable,
        offset:   usize,
        expression: Expression
    },

    Return {
        expression: Option<Expression>
    },

    ForLoop {
        assignment:     Box<Statements>,
        condition:      Expression,
        re_assignment:  Box<Statements>,
        instructions:   Vec<Statements>
    },

    WhileLoop {
        condition:      Expression,
        instructions:   Vec<Statements>
    },

    IfChain {
        blocks:         Vec<IfBlock>
    }
}