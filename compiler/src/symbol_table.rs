use crate::parse_tree;
use crate::data_settings;

#[derive(Debug, Clone)]
pub(crate) struct FunctionDefinition{ 
    pub(crate) name: String,
    pub(crate) parameters:   Vec<parse_tree::VariableTypePair>,
    pub(crate) return_type:  parse_tree::Types,
    pub(crate) location:     parse_tree::Location,
    pub(crate) file:         String
}

#[derive(Debug, Clone)]
pub(crate) struct StructDefinition{ 
    pub(crate) name:     String, 
    pub(crate) members:  Vec<parse_tree::VariableTypePair>,
    pub(crate) location: parse_tree::Location,
    pub(crate) size:     usize,
    pub(crate) file:     String
}

#[derive(Debug, Clone)]
pub(crate) struct VariableDefinition{
    pub(crate) variable_name: String, 
    pub(crate) assigned_type: parse_tree::Types,
    pub(crate) location:      parse_tree::Location,
    pub(crate) variable_size: usize,
    pub(crate) file:     String
}

pub(crate) enum TableEntry {
    Function(FunctionDefinition),
    Struct(StructDefinition),
    Variable(VariableDefinition)
}

pub(crate) struct SymbolTable {

    global:        Vec<TableEntry>,
    scopes:        Vec<Vec<TableEntry>>,
}

impl SymbolTable {

    pub(crate) fn new() -> Self {
        SymbolTable{
            global: Vec::new(),
            scopes: Vec::new()
        }
    }
    
    pub(crate) fn new_scope(&mut self) {
        self.scopes.push(Vec::new());
    }

    pub(crate) fn pop_scope(&mut self) {
        self.scopes.pop();
    }

    pub(crate) fn add_global_function(&mut self, 
        file: String,
        name: String,
        parameters:  Vec<parse_tree::VariableTypePair>,
        return_type:  parse_tree::Types,
        location: parse_tree::Location ) {
            self.global.push(TableEntry::Function(

                FunctionDefinition{
                    file: file,
                    name    : name,
                    parameters  : parameters,
                    return_type : return_type,
                    location    : location
                }
            ));
    }

    pub(crate) fn add_global_struct(&mut self,
        file: String,
        name: String, 
        members: Vec<parse_tree::VariableTypePair>,
        location: parse_tree::Location) {

            let mut size_calc : usize = 0;

            for item in members.iter() {
                if let Some(value) = self.calculate_size(&item.data_type) {
                    size_calc += value;
                } else {
                    panic!("Unable to calculate size of given structure : {}", name); 
                }
            }

            self.global.push(TableEntry::Struct(

                StructDefinition{
                    file: file,
                    name: name,
                    members: members,
                    size:    size_calc,
                    location: location
                }
            ));
    }

    pub(crate) fn calculate_size(&self, assigned_type: &parse_tree::Types) -> Option<usize> {

        let size_required = match &*assigned_type {

            parse_tree::Types::Integer => {
                Some(data_settings::INTEGER_SIZE)
            },
            parse_tree::Types::Float => {
                Some(data_settings::FLOAT_SIZE)
            },
            parse_tree::Types::Char => {
                Some(data_settings::CHAR_SIZE)
            },
            parse_tree::Types::Nil => {
                Some(data_settings::NIL_SIZE)
            },

            parse_tree::Types::Struct{ name } => {

                match self.get_global_struct(&name) {
                    Some(s) => {

                        let mut struct_size : usize = 0;

                        //  Recursive (infinite size) struct definitions should be weeded out by now
                        //
                        for item in s.members.iter() {

                            if let Some(value) = self.calculate_size(&item.data_type) {

                                struct_size += value;

                            } else {
                                return None;
                            }
                        }

                        Some(struct_size)
                    },
                    None => {
                        return None;
                    }
                }
            }
        };

        size_required
    }

    pub(crate) fn add_local_variable(&mut self,
        variable_name: String, 
        assigned_type: parse_tree::Types,
        location:      parse_tree::Location,
        file:          String) {


            // Increase stack index by amount required to hold item
            let size = match self.calculate_size(&assigned_type) {
                Some(size) => size,
                None => {
                    panic!("Unable to determine size of object : {} >> {:?}", variable_name, assigned_type);
                }
            };

            // Add variable to scope with address
            if let Some(scope) = self.scopes.last_mut() {
                scope.push(TableEntry::Variable(

                    VariableDefinition{
                        variable_name: variable_name,
                        assigned_type: assigned_type,
                        location: location,
                        variable_size: size.clone(),
                        file: file
                    }
                ));
            } else {
                panic!("Internal Error : Symbol table unable to retrieve scope for add_local_variable");
            }
    }

    pub(crate) fn get_global_function(&self, name: &String) -> Option<FunctionDefinition> {
        for item in self.global.iter() {
            match item {
                TableEntry::Function( f ) => {
                    if f.name == *name {
                        return Some(f.clone());
                    }
                },
                _ => {}
            }
        }
        None
    }

    pub(crate) fn get_global_struct(&self, name: &String) -> Option<StructDefinition> {
        for item in self.global.iter() {
            match item {
                TableEntry::Struct( s ) => {
                    if s.name == *name {
                        return Some(s.clone());
                    }
                },
                _ => {}
            }
        }
        None
    }

    pub(crate) fn get_local_variable(&self, name: &String) -> Option<VariableDefinition> {

        for scope in self.scopes.iter().rev() {
            for item in scope.iter() {
                match item {
                    TableEntry::Variable( v ) => {
                        if v.variable_name == *name {
                            return Some(v.clone());
                        }
                    },
                    _ => {}
                }
            }
        }

        None
    }

}