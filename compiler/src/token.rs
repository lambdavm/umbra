
/*
    Tokens for the language, the tokens with (usize) are used for encoding the position
    of the token start position. This is so we can encode statement positions into the ast.
    This is for semantic analysis and better error reporting
*/

use derive_more::{ Display };

#[derive(Display, Copy, Clone, Debug, PartialEq)]
pub enum Tok <'input> {
    
    #[display(fmt = "Let")]
    Let(usize),
    #[display(fmt = "Set")]
    Set(usize),
    Arrow,
    LeftArrow,

    TypeNil,
    TypeInteger,
    TypeFloat,
    TypeChar,
    #[display(fmt = "Function")]
    TypeFunction(usize),

    #[display(fmt = "Struct")]
    TypeStruct(usize),

    #[display(fmt = "For")]
    For(usize),
    
    #[display(fmt = "While")]
    While(usize),


    #[display(fmt = "If")]
    If(usize),

    #[display(fmt = "Elif")]
    Elif(usize),

    #[display(fmt = "Else")]
    Else(usize),

    Comma,

    #[display(fmt = "Include")]
    Include(usize),

    #[display(fmt = "{{")]
    LeftCurly(usize),

    #[display(fmt = "}}")]

    RightCurly(usize),
    LeftParen,
    RightParen,

    #[display(fmt = "[")]
    LeftBracket(usize),

    #[display(fmt = "]")]
    RightBracket(usize),

    #[display(fmt = ":")]
    Colon(usize),
    Dot,

    #[display(fmt = ";")]
    SemiColon(usize),
    Mul,
    Div,
    Pow,
    Mod,
    Lsh,
    Rsh,
    Xor,
    BwOr,
    BwAnd,
    Or,
    And,
    Add,
    Sub,
    Lte,
    Gte,
    Gt,
    Lt,
    Ne,
    Comparison,

    #[display(fmt = "::")]
    Eyes(usize),
    Negate,
    Tilde,

    #[display(fmt = "=")]
    Equal(usize),
    
    Float(f64),
    Integer(i64),
    String(&'input str),
    Identifier(&'input str),
    StringLiteral(&'input str),
    Char(&'input str),
}