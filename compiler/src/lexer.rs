use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::mem;
use std::str::CharIndices;

use super::token;

#[inline]
fn is_id_start(ch: char) -> bool {
    ch == '_' || ch.is_ascii_alphabetic()
}

#[inline]
fn is_id_continue(ch: char) -> bool {
    ch == '_' || ch.is_ascii_digit()
}

pub type Location = usize;

#[derive(Debug, PartialEq)]
pub enum LexicalError {
    InvalidCharacter { ch: char, location: Location },
    UnterminatedString { location: Location },
}

impl Display for LexicalError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            LexicalError::InvalidCharacter { ch, location } => {
                write!(f, "Invalid character '{}' found at {}", ch, location)
            }
            LexicalError::UnterminatedString { location } => {
                write!(f, "String starting at {} was not terminated", location)
            }
        }
    }
}

impl Error for LexicalError {}

pub type SpanResult<'input> = Result<(Location, token::Tok<'input>, Location), LexicalError>;

pub struct Lexer<'input> {
    source: &'input str,
    chars: CharIndices<'input>,
    lookahead: Option<(usize, char)>,
    lookahead2: Option<(usize, char)>,
}

impl<'input> Lexer<'input> {
    pub fn new(source: &'input str) -> Lexer<'input> {
        let mut chars = source.char_indices();
        let lookahead = chars.next();
        let lookahead2 = chars.next();

        Lexer {
            source,
            chars,
            lookahead,
            lookahead2,
        }
    }

    fn bump(&mut self) -> Option<(usize, char)> {
        mem::replace(
            &mut self.lookahead,
            mem::replace(&mut self.lookahead2, self.chars.next()),
        )
    }

    fn take_until<F>(&mut self, mut terminate: F) -> Option<usize>
    where
        F: FnMut(char) -> bool,
    {
        while let Some((i, ch)) = self.lookahead {
            if terminate(ch) {
                return Some(i);
            } else {
                self.bump();
            }
        }

        None
    }

    fn take_while<F>(&mut self, mut condition: F) -> Option<usize>
    where
        F: FnMut(char) -> bool,
    {
        self.take_until(|ch| !condition(ch))
    }

    fn skip_to_line_end(&mut self) {
        self.take_while(|ch| ch != '\n');
    }

    fn skip_whitespace(&mut self) {
        self.take_while(|ch| ch.is_whitespace());
    }

    fn read_string(&mut self, pos: usize) -> SpanResult<'input> {
        match self.take_until(|ch| ch == '"') {
            Some(i) => {
                self.bump();
                Ok((pos, token::Tok::String(&self.source[pos + 1..i]), i + 1))
            }
            None => Err(LexicalError::UnterminatedString { location: pos }),
        }
    }

    fn read_string_literal(&mut self, pos: usize) -> SpanResult<'input> {
        match self.take_until(|ch| ch == '\'') {
            Some(i) => {
                self.bump();
                if i - pos != 2 {
                    Ok((pos, token::Tok::StringLiteral(&self.source[pos + 1..i]), i + 1))
                } else {
                    Ok((pos, token::Tok::Char(&self.source[pos + 1..i]), i + 1))
                }
            }
            None => Err(LexicalError::UnterminatedString { location: pos }),
        }
    }

    fn read_number(&mut self, pos: usize) -> SpanResult<'input> {
        let mut end = self.take_while(|ch| ch.is_ascii_digit());

        let mut as_integer = true;

        if let Some((_, '.')) = self.lookahead {

            as_integer = false;

            // Check if it's a decimal or a field access
            if let Some((_, next_ch)) = self.lookahead2 {
                if next_ch.is_ascii_digit() {
                    self.bump();
                    end = self.take_while(|ch| ch.is_ascii_digit());
                }
            }
        }

        let end = end.unwrap_or_else(|| self.source.len());

        if as_integer {
            Ok((
                pos,
                token::Tok::Integer(self.source[pos..end].parse().expect("unparsable integer")),
                end,
            ))
        } else {
            Ok((
                pos,
                token::Tok::Float(self.source[pos..end].parse().expect("unparsable float")),
                end,
            ))
        }
    }

    fn read_identifier(&mut self, pos: usize) -> SpanResult<'input> {
        let end = self
            .take_while(|ch| is_id_start(ch) || is_id_continue(ch))
            .unwrap_or_else(|| self.source.len());

        let token = match &self.source[pos..end] {
            "nil"    => token::Tok::TypeNil,
            "int"    => token::Tok::TypeInteger,
            "float"  => token::Tok::TypeFloat,
            "def"    => token::Tok::TypeFunction(pos),
            "struct" => token::Tok::TypeStruct(pos),
            "char"   => token::Tok::TypeChar,
            "let"    => token::Tok::Let(pos),
            "set"    => token::Tok::Set(pos),
            "for"    => token::Tok::For(pos),
            "while"  => token::Tok::While(pos),
            "if"     => token::Tok::If(pos),
            "elif"   => token::Tok::Elif(pos),
            "else"   => token::Tok::Else(pos),
            "pow"    => token::Tok::Pow,
            "include"=> token::Tok::Include(pos),
            id       => token::Tok::Identifier(id),
        };

        Ok((pos, token, end))
    }
}

impl<'input> Iterator for Lexer<'input> {
    type Item = SpanResult<'input>;

    fn next(&mut self) -> Option<SpanResult<'input>> {
        self.skip_whitespace();

        if let Some((i, ch)) = self.bump() {
            match ch {
                '{' => Some(Ok((i, token::Tok::LeftCurly(i), i + 1))),
                '}' => Some(Ok((i, token::Tok::RightCurly(i), i + 1))),
                '(' => Some(Ok((i, token::Tok::LeftParen, i + 1))),
                ')' => Some(Ok((i, token::Tok::RightParen, i + 1))),
                '[' => Some(Ok((i, token::Tok::LeftBracket(i), i + 1))),
                ']' => Some(Ok((i, token::Tok::RightBracket(i), i + 1))),
                ';' => Some(Ok((i, token::Tok::SemiColon(i), i + 1))),
                ':' => {
                    if let Some((_, ':')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Eyes(i), i + 2)))
                    } else {
                        Some(Ok((i, token::Tok::Colon(i), i + 1)))
                    }
                },
                ',' => Some(Ok((i, token::Tok::Comma, i + 1))),
                '.' => Some(Ok((i, token::Tok::Dot, i + 1))),
                '+' => Some(Ok((i, token::Tok::Add, i + 1))),
                '%' => Some(Ok((i, token::Tok::Mod, i + 1))),
                '~' => Some(Ok((i, token::Tok::Tilde, i + 1))),
                '^' => Some(Ok((i, token::Tok::Xor, i + 1))),
                '-' => {
                    if let Some((_, '>')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Arrow, i + 2)))
                    } else {
                        Some(Ok((i, token::Tok::Sub, i + 1)))
                    }
                },
                '*' => { Some(Ok((i, token::Tok::Mul, i + 1))) },
                '/' => {
                    if let Some((_, '/')) = self.lookahead {
                        self.skip_to_line_end();
                        self.next()
                    } else {
                        Some(Ok((i, token::Tok::Div, i + 1)))
                    }
                }

                '!' => {
                    if let Some((_, '=')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Ne, i + 2)))
                    } else {
                        Some(Ok((i, token::Tok::Negate, i + 1)))
                    }
                }

                '=' => {
                    if let Some((_, '=')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Comparison, i + 2)))
                    } else {
                        Some(Ok((i, token::Tok::Equal(i), i + 1)))
                    }
                }

                '>' => {
                    if let Some((_, '=')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Gte, i + 2)))
                    }
                    else if let Some((_, '>')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Rsh, i + 2)))
                    }  else {
                        Some(Ok((i, token::Tok::Gt, i + 1)))
                    }
                }

                '<' => {
                    if let Some((_, '=')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Lte, i + 2)))
                    } else if let Some((_, '-')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::LeftArrow, i + 2)))
                        
                    }
                    else if let Some((_, '<')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Lsh, i + 2)))
                    }  else {
                        Some(Ok((i, token::Tok::Lt, i + 1)))
                    }
                }

                '&' => {
                    if let Some((_, '&')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::And, i + 2)))
                    } else {
                        Some(Ok((i, token::Tok::BwAnd, i + 1)))
                    }
                }

                '|' => {
                    if let Some((_, '|')) = self.lookahead {
                        self.bump();
                        Some(Ok((i, token::Tok::Or, i + 2)))
                    } else {
                        Some(Ok((i, token::Tok::BwOr, i + 1)))
                    }
                }

                '"' => Some(self.read_string(i)),
                '\'' => Some(self.read_string_literal(i)),
                ch if is_id_start(ch) => Some(self.read_identifier(i)),
                ch if ch.is_ascii_digit() => Some(self.read_number(i)),

                ch => Some(Err(LexicalError::InvalidCharacter { ch, location: i })),
            }
        } else {
            None
        }
    }
}

/*
    ----------------------------------------------------------------------------------

                                    TESTS

    ----------------------------------------------------------------------------------
*/

#[cfg(test)]
mod test {
    use super::*;
    use super::token::Tok;

    fn lex(source: &str, expected: Vec<(usize, Tok<'_>, usize)>) {
        let mut lexer = Lexer::new(source);

        let mut actual_len = 0;
        let expected_len = expected.len();

        for (expected, actual) in expected.into_iter().zip(lexer.by_ref()) {
            actual_len += 1;
            let actual = actual.unwrap();
            assert_eq!(expected.0, actual.0);
            assert_eq!(expected.1, actual.1);
            assert_eq!(expected.2, actual.2);
        }

        assert_eq!(expected_len, actual_len);
        assert_eq!(None, lexer.next());
    }

    #[test]
    fn delimiters() {
        lex(
            "{} [] ()",
            vec![
                (0, Tok::LeftCurly(0), 1),
                (1, Tok::RightCurly(1), 2),
                (3, Tok::LeftBracket(3), 4),
                (4, Tok::RightBracket(4), 5),
                (6, Tok::LeftParen, 7),
                (7, Tok::RightParen, 8),
            ],
        );
    }

    #[test]
    fn operators() {
        lex(
            ", . + - * / = == ! != > >= < <= && || pow % | ^ & << >> ~",
            vec![
                (0,  Tok::Comma, 1),
                (2,  Tok::Dot, 3),
                (4,  Tok::Add, 5),
                (6,  Tok::Sub, 7),
                (8,  Tok::Mul, 9),
                (10, Tok::Div, 11),
                (12, Tok::Equal(12), 13),
                (14, Tok::Comparison, 16),
                (17, Tok::Negate, 18),
                (19, Tok::Ne, 21),
                (22, Tok::Gt, 23),
                (24, Tok::Gte, 26),
                (27, Tok::Lt, 28),
                (29, Tok::Lte, 31),
                (32, Tok::And, 34),
                (35, Tok::Or, 37),
                (38, Tok::Pow, 41),
                (42, Tok::Mod, 43),
                (44, Tok::BwOr, 45),
                (46, Tok::Xor, 47),
                (48, Tok::BwAnd, 49),
                (50, Tok::Lsh, 52),
                (53, Tok::Rsh, 55),
                (56, Tok::Tilde, 57)
            ],
        );
    }

    #[test]
    fn line_comment() {
        lex(
            "123; // comment\n 123",
            vec![
                (0, Tok::Integer(123), 3),
                (3, Tok::SemiColon(3), 4),
                (17, Tok::Integer(123), 20),
            ],
        );
    }

    #[test]
    fn string() {
        lex(
            "\"hello, world\"",
            vec![(0, Tok::String("hello, world"), 14)],
        );
    }

    #[test]
    fn string_lit() {
        lex(
            "\'hello, world\'",
            vec![(0, Tok::StringLiteral("hello, world"), 14)],
        );
    }

    #[test]
    fn integer() {
        lex("123", vec![(0, Tok::Integer(123), 3)]);
    }

    #[test]
    fn float() {
        lex("123.45", vec![(0, Tok::Float(123.45), 6)]);
    }

    #[test]
    fn identifiers() {
        lex("id", vec![(0, Tok::Identifier("id"), 2)]);
        lex("_id", vec![(0, Tok::Identifier("_id"), 3)]);
        lex("id123", vec![(0, Tok::Identifier("id123"), 5)]);
    }

    #[test]
    fn keywords() {
        lex("int"     , vec![ (0, Tok::TypeInteger, 3)]);
        lex("float"   , vec![ (0, Tok::TypeFloat,   5)]);
        lex("nil"     , vec![ (0, Tok::TypeNil,     3)]);
    }
}