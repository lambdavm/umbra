
#[macro_use]
extern crate verbosity;
use verbosity::Verbosity;

extern crate source;
use source::{ Source, SourceError, Page };

mod symbol_table;
use symbol_table::SymbolTable;

mod semantic_errors;
use semantic_errors::SemanticError;

mod code_tree_optimizer;

extern crate code_gen;

mod vm_target;

#[macro_use] 
extern crate lalrpop_util;

lalrpop_mod!(pub language);

extern crate derive_more;

pub (crate) mod data_settings;

pub mod parse_tree;
use parse_tree::ParseTree;

mod code_tree;
mod code_scanner;

pub mod lexer;
pub mod token;
mod error_reporter;

use std::collections::HashSet;
use ansi_term::Colour::{Yellow, Purple};
use std::time::{Duration, Instant};

/// The resulting data from a compilation
#[derive(Debug)]
pub struct CompiledResult {
    pub time_elapsed: Duration,
    pub byte_code:    Vec<u8>
}

/// The compiler
pub struct Compiler <'a> {

    v: &'a Verbosity,
    source_manager: &'a Source<'a>,
    parse_trees:        Vec<ParseTree>,
    imported_files:     HashSet<String>
}

impl <'a> Compiler <'a> {

    /// Create a new compiler object
    pub fn new(v: &'a Verbosity, source_manager: &'a Source) -> Self {
        Self {
            v: v,
            source_manager: source_manager,
            parse_trees:    Vec::new(),
            imported_files: HashSet::new()
        }
    }

    /// Given the source the compiler was given on ::new, this will attempt to compile
    /// the source code and result in a CompiledResult object. If nothing is returned,
    /// an error has cropped up and will be displayed to the user
    pub fn compile(&mut self) -> Option<CompiledResult> {

        let start_time = Instant::now();

        let entry_file = match self.source_manager.get_entry_file_ref() {
            Ok(e_file) => e_file,
            Err(e)     => {
                error_reporter::report_source_manager_error(e, &self.source_manager, &"".to_string(), 0);
                return None;
            }
        };

        //  Build all of the parse trees from the source files
        //
        if !self.build_parse_trees(&entry_file) {
            return None;
        }

        //  Check the semantics of each parse tree and convert them into code trees
        //
        let mut code_scanner = code_scanner::CodeScanner::new(&self.v);

        let mut code_trees = match code_scanner.scan(&mut self.parse_trees) {
            Ok(trees) => {
                trees 
            }, 
            Err(semantic_errors) => {
                for error in semantic_errors.iter() {
                    error_reporter::report_semantic_error(error, &self.source_manager);
                }
                return None;
            }
        };

        //  Code trees should be 100% valid. If they aren't its a failure on us.

        //  Attempt to do some optimizations on the code trees
        //
        code_tree_optimizer::optimize(&mut code_trees);

        // Generate the VM code
        //
        let byte_code = match vm_target::generate_lvm_bytecode(&self.v, &code_trees) {
            Ok(generated_code) => {
                generated_code
            },
            Err(e) => {
                eprintln!("Generator Error : {}", e);
                return None;
            }
        };

        //  Return the generated code
        //
        return Some(CompiledResult{
            time_elapsed: start_time.elapsed(),
            byte_code:    byte_code
        });
    }

    /// Build a source tree using lalrpop. Any errors will be displayed and result in a 'None' return
    fn build_parse_trees(&mut self, file_name: &String) -> bool {

        // Entry file contents
        let page_file : &Page = match self.source_manager.get_page_ref(file_name) {
            Ok(page) => {
                page
            },
            Err(e) => {
                error_reporter::report_source_manager_error(e, &self.source_manager, &"".to_string(), 0);
                return false;
            }
        };

        //page_file: &Page
        let (source_file, program_code) : (String, String) = page_file.get_processable();

        let mut parse_tree = match language::ProgramParser::new().parse(
            lexer::Lexer::new(program_code.as_str())) {
            Ok(tree)  => { 
                tree
            }

            Err(lalrpop_error) => {
                error_reporter::report_lalrpop_error(source_file, &self.source_manager, lalrpop_error);
                return false;
            }
        };

        parse_tree.origin_file = source_file.clone();
                
        vhigh!(self.v, vfunc!(), format!("Program Parse Tree: \n{:?}", parse_tree).as_str());

        // Handle the import
        for import in parse_tree.includes.iter() {

            if ! self.imported_files.contains(import) {

                if ! self.build_parse_trees(&import) {
                    return false;
                }

                self.imported_files.insert(import.clone());
            } else {

                vlow!(self.v, vfunc!(), format!("{}: File \"{}\" included multiple times", Purple.paint("Info:"), Yellow.paint(import) ).as_str());
                vlow!(self.v, vfunc!(), format!("File currently including it is : {}", Yellow.paint(file_name.clone())).as_str());
            }
        }

        self.parse_trees.push(parse_tree);

        return true;
    }

}

