/*

    About:

        This file defines the structures that will be generated from the lalrpop grammar and passed into the code to be analyzed and then 
        reshaped into a more strictly defined code tree that will be used for optimization and code generation

*/

use derive_more::{ Display };

#[derive(Debug, Clone, PartialEq)]
pub struct ParseTree {
    pub statements    : Vec<TopLevelStatements>,
    pub includes      : Vec<String>,
    pub origin_file   : String
}

#[derive(Debug, Clone, PartialEq)]
pub struct Location {
    pub start: usize,
    pub end:   usize
}

#[derive(Display, Debug, Clone, PartialEq, std::cmp::Eq, std::hash::Hash)]
pub enum Types {

    #[display(fmt = "Integer")]
    Integer,

    #[display(fmt = "Float")]
    Float,

    #[display(fmt = "Char")]
    Char,

    #[display(fmt = "Struct<{}>", name)]
    Struct{ name: String },

    #[display(fmt = "Nil")]
    Nil,
}

#[derive(Debug, Clone, PartialEq)]
pub enum TopLevelStatements {

    StructDefinition{ 
        name: String, 
        members: Vec<VariableTypePair>,
        location: Location
    },

    FunctionDefinition{ 
        name: String,
        parameters:  Vec<VariableTypePair>,
        instructions: Vec<Statements>,     
        return_type:  Types,
        location: Location 
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Variable {
    pub name: String,
    pub accessors: Option<Vec<Expr>>
}

#[derive(Debug, Clone, PartialEq)]
pub enum Statements {                // Statements that can be found in a method

    BuiltInFunction{
        function_name: String,
        params: Vec<Box<Expr>>,
        location: Location 
    },

    FunctionCall{
        function_name: String,
        params: Vec<Box<Expr>>,
        location: Location 
    },

    Assignment{
        variable:   Variable, 
        assigned_type: Types,
        expression:    Expr,
        location: Location 
    },

    Reassignment{
        variable:   Variable, 
        expression:    Expr,
        location: Location 
    },

    DotReassignment{
        variable:      Variable, 
        accessor_list: Vec<String>,
        expression:    Expr,
        location: Location 
    },

    ForLoop {
        definition: Box<ForLoop>,
        location: Location 
    },

    WhileLoop {
        definition: Box<WhileLoop>,
        location: Location 
    },

    IfStatement {
        blocks: Vec<IfBlock>,
        location: Location 
    },

    Return{
        expr: Option<Expr>,
        location: Location 
    }
}

#[derive(Debug, Display, Clone, PartialEq)]
#[display(fmt = "Variable: {}", variable)]
pub struct VariableTypePair {
    pub variable:   String,
    pub data_type:  Types
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionCall { 
    pub name: String,         
    pub params: Vec<Box<Expr>>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ForLoop {
    pub assignment: Statements,
    pub condition:  Expr,
    pub re_assignment: Statements,
    pub instructions: Vec<Statements>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct WhileLoop {
    pub condition:  Expr,
    pub instructions: Vec<Statements>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct IfBlock {
    pub condition: Expr,
    pub instructions: Vec<Statements>,
    pub location: Location 
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {                      // Expressions
    Nil,
    Integer{ value: i64 },
    Float{ value: f64 },
    Char { value: String },
    Identifier{ value: Variable },
    Accessor{ value: String, list: Vec<String>},
    Op{ lhs: Box<Expr>, op: Opcode, rhs: Box<Expr>},
    UnaryOp{ op: UnaryOpcode, rhs: Box<Expr>},
    FunctionCall{ function: FunctionCall},

    ExprList{ name: String, list: Vec<Box<Expr>> }
}

#[derive(Display, Debug, Clone, PartialEq)]
pub enum Opcode {
    Mul,
    Div,
    Add,
    Sub,
    Lte,
    Gte,
    Gt,
    Lt,
    Equal,
    Ne,
    Pow,
    Mod,
    Lsh,
    Rsh,
    BwXor,
    BwOr,
    BwAnd,
    Or,
    And
}

#[derive(Display, Debug, Clone, PartialEq)]
pub enum UnaryOpcode {
    Negate,
    BwNot
}