
use std::path::Path;

extern crate derive_more;

#[macro_use]
extern crate verbosity;
use verbosity::Verbosity;

use derive_more::{ Display };

mod page;
pub use page::Page;

use std::collections::{ HashMap, HashSet };

/// Errors thrown by a Source object
#[derive(Display, Debug)]
pub enum SourceError {

    #[display(fmt = "Error attempting to load source file : {}", file )]
    UnableToLoadSource{ file: String },

    #[display(fmt = "Requested page doesn't exist : {}", page )]
    RequestedPageDoesntExist{ page: String },

    #[display(fmt = "No entry file present")]
    NoEntryFilePresent,
}

/// The object that tracks all source files, and handles the ownership
/// of processed sources (pages) that can be indexed to retrieve lines for
/// error generation / parser reporter
pub struct Source <'a> {

    v: &'a Verbosity,       // Display output if requested

    pages: HashMap<String, Page<'a>>,   // Loaded source files 

    imported_files: HashSet<String>,

    entry_file: Option<String>
}

impl <'a> Source <'a> {

    pub fn new(v: &'a Verbosity) -> Self {
        Self {
            v: v,
            pages: HashMap::new(),
            imported_files: HashSet::new(),
            entry_file: None
        }
    }

    /// Attempt to load a source file
    pub fn load_source_file(&mut self, file: &String) -> Option<SourceError> {

        // Check if its already been loaded, if it has return as-if its been loaded
        if self.imported_files.contains(file) { 
            return None;
        }

        // Attempt to load the source file!
        match Page::from_file(self.v, file) {

            Ok(page) => { 
                
                // The path was a file and we read it in - so now we just take the file name
                // and go with it
                let file_name = Path::new(& file).file_name().unwrap().to_str().unwrap().to_string();

                if self.pages.contains_key(&file_name.clone()) { 
                    return None;
                }

                // If there isn't a declared entry file, it will be the first we see
                if self.entry_file.is_none() {
                    self.entry_file = Some(file_name.clone());
                }

                // Place the new page into the pages hashmap 
                self.pages.insert(file_name.clone(), page);
                self.imported_files.insert(file.clone());
                return None;
             }

            Err(e) => {
                vlow!(self.v, vfunc!(), format!("Page Error: {}", e).as_str());
                return Some(SourceError::UnableToLoadSource{ file: file.clone() });
             }
        }
    }

    /// Get loaded source object
    pub fn get_entry_file_ref(&self) -> Result<String, SourceError> {

        if self.entry_file.is_none() {
            return Err(SourceError::NoEntryFilePresent);
        }

        let file : &String = self.entry_file.as_ref().unwrap();

        return Ok(file.clone());
    }

    /// Get loaded source object
    pub fn get_page_ref(&self, file_name: &String) -> Result<&Page, SourceError> {

        if self.pages.contains_key(file_name) {
            return Ok(self.pages.get(file_name).unwrap());
        } else {

            // If the requested page wasn't found, attempt to find it with just the file name
            let file_name = Path::new(& file_name).file_name().unwrap().to_str().unwrap().to_string();
            if self.pages.contains_key(&file_name) {
                return Ok(self.pages.get(&file_name).unwrap());
            }
        }

        return Err(SourceError::RequestedPageDoesntExist{ page: file_name.clone() });
    }

    /// Check if source has been loaded yet
    pub fn source_has_been_loaded(&self, file: &String) -> bool {

        if self.pages.contains_key(file) { 
            return true;
        }

        return false;
    }
}