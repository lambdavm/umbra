use std::fs::File;
use std::io::{BufRead, BufReader};

use derive_more::{ Display };

use verbosity::Verbosity;

/// Page Errors
#[derive(Display, Debug)]
pub enum PageError {

    #[display(fmt = "Error attempting to load source file : {}", file )]
    UnableToLoadPage{ file: String },

    #[display(fmt = "Error attempting to read source ({}) line ({})", file, line )]
    UnableToReadLine{ file: String, line: usize },
}

/// A representation of a line, with byte placement data for pulling lines from byte index
#[derive(Debug, Clone)]
struct Line {
    number: usize,
    start: usize,
    end: usize,
    contents: String
}

/// A loaded source file ready to be parsed and processed.
/// This structure is also used to pull lines ranges based on byte index 
/// from the processable string
pub struct Page <'a> {
    v: &'a Verbosity,       // Display output if requested
    process_able: String,
    lines: Vec<Line>,
    file: String
}

impl <'a> Page <'a> {

    /// Load a page from file
    pub fn from_file(v: &'a Verbosity, file: &String) -> Result<Page<'a>, PageError> {

        vmed!(v, vfunc!(), format!("Attempting to load page : {}", file).as_str());

        let file_in = match File::open(file) {
            Ok(o)  => o,
            Err(_) => { return Err(PageError::UnableToLoadPage{ file: file.clone() });}
        };

        // Reader
        let reader = BufReader::new(file_in);
    
        let mut conjoined_input : String = String::new();
    
        let mut line_vector : Vec<Line> = Vec::new();

        // Iter over lines and build things
        for (index, line) in reader.lines().enumerate() {
    
            let line = match line {
                Ok(l) => l,
                Err(e) => { 
                    vhigh!(v, vfunc!(), format!("Error reading page line ({}) : {}", index, e).as_str());
                    return Err(PageError::UnableToReadLine{ line: index,  file: file.clone() });
                }
            };
    
            // Store line info for errors
            line_vector.push(Line{
                number: index+1,        // We want to 1-index not 0-index
                start:  conjoined_input.len(),
                end:    conjoined_input.len() + line.len(), // add one for newline
                contents: line.clone().trim_start().trim_end().to_string()
            });

            // Concat for processable
            conjoined_input = conjoined_input + "\n" + line.as_str();
        }

        vmed!(v, vfunc!(), format!("Page loaded : {}", file).as_str());

        Ok(Self{
            process_able: conjoined_input,
            lines: line_vector,
            v: v,
            file: file.clone()
        })
    }

    /// Get the processable data
    pub fn get_processable(&self) -> (String, String) {
        return (self.file.clone(), self.process_able.clone())
    }

    /// Get line number of a single byte
    pub fn get_line_no(&self, index: usize) -> Option<usize> {

        for line in self.lines.iter() {
            if index >= line.start && index-1 <= line.end {
                return Some(line.number);
            }
        }

        vmed!(self.v, vfunc!(), format!("Unable to locate line by index : {}", index).as_str());
        return None;
    }

    /// Get the position of where the line starts in the source string based on the index
    /// of a given token
    pub fn get_line_start_pos_based_on_token(&self, index: usize) -> Option<usize> {

        for line in self.lines.iter() {
            if index >= line.start && index-1 <= line.end {
                return Some(line.start);
            }
        }

        vmed!(self.v, vfunc!(), format!("Unable to locate line by index : {}", index).as_str());
        return None;
    }


    /// Get line contents
    pub fn get_line_contents(&self, start: usize, end: usize) -> Option<String> {

        for line in self.lines.iter() {

            // println!("Looking for ({}, {}) >>> ({}, {})", start, end, line.start, line.end);
             if start >= line.start &&  end-1 <= line.end {
                 return Some(line.contents.clone())
             }
         }

         vmed!(self.v, vfunc!(), format!("Unable to locate line by range : ({}, {})", start, end).as_str());
         return None;
    }
}