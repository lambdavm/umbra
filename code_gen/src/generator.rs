
use crate::Instructions;
use crate::lvm;

use std::fs::File;
use std::io::prelude::*;
use std::io::LineWriter;

#[inline(always)]
pub fn generate_bytecode(instructions: Vec<Instructions>) -> Vec<u8> {

  //  println!("Generator not yet complete - got {} instructions", instructions.len());

    /*
    
        Here we will step over the instructions given and output some .lvm code. For development this should be written
        out to a file before its piped into the assembler so we can analyze it. 

        Once we create it here though we will pipe it write to the assembler, and then return the given byte code.


        We will also have to "bootstrap" the software by defining some random entry method that actually calls main. 
            This should be done so later we can add funky stuff to that method that sets things up for the VM.
    
    */

    let mut asm = Vec::<String>::new();

    asm.push(format!(".init __bootstrap__"));
    asm.push(format!("\n.code\n"));
    asm.push(format!("__bootstrap__:"));
    asm.push(format!("\n\tjmp main"));
    asm.push(format!("\n\texit\n"));

    for ins in instructions.iter() {

        asm.push(format!("\n"));

        match ins {

            Instructions::Comment{ data } => {
                asm.push(format!("\t; {}", data));
            }

            Instructions::Label{ name } => {
                asm.push(format!("{}:", name));
            }

            Instructions::Goto{ label } => {
                asm.push(format!("\tgoto {}", label));
            }

            Instructions::Call{ name } => {
                asm.push(format!("\tjmp {}", name));
            }

            Instructions::Return => {
                asm.push(format!("\tret"));
            }

            Instructions::ExpandStack{ source } => {
                asm.push(format!("\tes r{}", source));
            }

            Instructions::MarkStack => {
                asm.push(format!("\tms"));
            }

            Instructions::ResetStack => {
                asm.push(format!("\trs"));
            }

            Instructions::Fp{ dest } => {
                asm.push(format!("\tfp r{}", dest));
            }

            Instructions::Ufp{ source } => {
                asm.push(format!("\tufp r{}", source));
            }

            Instructions::StackSize{ dest } => {
                asm.push(format!("\tsize r{}", dest));
            }

            Instructions::PushWord{ source } => {
                asm.push(format!("\tpushw r{}", source));
            }

            Instructions::PushHalf{ source } => {
                asm.push(format!("\tpushh r{}", source));
            }

            Instructions::PushByte{ source } => {
                asm.push(format!("\tpushb r{}", source));
            }

            Instructions::PopWord{ dest } => {
                asm.push(format!("\tpopw r{}", dest));
            }

            Instructions::PopByte{ dest } => {
                asm.push(format!("\tpopb r{}", dest));
            }

            Instructions::PopHalf{ dest } => {
                asm.push(format!("\tpoph r{}", dest));
            }

            Instructions::MovValue{ dest, source } => {
                asm.push(format!("\tmov r{} ${}", dest, source));
            }

            Instructions::MovExact{ dest, source } => {
                asm.push(format!("\tmov r{} ${}", dest, source));
            }

            Instructions::MovAddress{ dest, source } => {
                asm.push(format!("\tmov r{} &{}", dest, source));
            }

            Instructions::MovRegister{ dest, source } => {
                asm.push(format!("\tmov r{} r{}", dest, source));
            }

            Instructions::Stb{ source, dest } => {
                asm.push(format!("\tstb r{} r{}", dest, source));
            }

            Instructions::Ldb{ source, dest } => {
                asm.push(format!("\tldb r{} r{}", dest, source));
            }

            Instructions::StoreStackWord{ dest_addr, source } => {
                asm.push(format!("\tstsw r{} r{}", dest_addr, source));
            }

            Instructions::StoreStackHalf{ dest_addr, source } => {
                asm.push(format!("\tstsh r{} r{}", dest_addr, source));
            }

            Instructions::StoreStackByte{ dest_addr, source } => {
                asm.push(format!("\tstsb r{} r{}", dest_addr, source));
            }

            Instructions::LoadStackWord{ source_addr, dest } => {
                asm.push(format!("\tldsw r{} r{}", dest, source_addr));
            }

            Instructions::LoadStackHalf{ source_addr, dest } => {
                asm.push(format!("\tldsh r{} r{}", dest, source_addr));
            }

            Instructions::LoadStackByte{ source_addr, dest } => {
                asm.push(format!("\tldsb r{} r{}", dest, source_addr));
            }

            Instructions::StoreMemoryByte{ dest_addr, source} => {
                asm.push(format!("\tstb r{} r{}", dest_addr, source));
            }
        
            Instructions::LoadMemoryByte{ source_addr, dest} => {
                asm.push(format!("\tldb r{} r{}", dest, source_addr));
            }

            Instructions::AssertEq{ lhs, rhs } => {
                asm.push(format!("\taseq r{} r{}", lhs, rhs));
            }

            Instructions::AssertNe{ lhs, rhs } => {
                asm.push(format!("\tasne r{} r{}", lhs, rhs));
            }

            Instructions::Add   => {
                asm.push(format!("\tadd r0 r2 r3"));
            }

            Instructions::AddSpecific { dest, lhs, rhs }   => {
                asm.push(format!("\tadd r{} r{} r{}", dest, lhs, rhs));
            }

            Instructions::Sub   => {
                asm.push(format!("\tsub r0 r2 r3"));
            }

            Instructions::Div   => {
                asm.push(format!("\tdiv r0 r2 r3"));
            }

            Instructions::Mul   => {
                asm.push(format!("\tmul r0 r2 r3"));
            }

            Instructions::Pow   => {
                asm.push(format!("\tpow r0 r2 r3"));
            }

            Instructions::Mod   => {
                asm.push(format!("\tmod r0 r2 r3"));
            }

            Instructions::AddF  => {
                asm.push(format!("\tadd.f r0 r2 r3"));
            }

            Instructions::SubF  => {
                asm.push(format!("\tsub.f r0 r2 r3"));
            }

            Instructions::DivF  => {
                asm.push(format!("\tdiv.f r0 r2 r3"));
            }

            Instructions::MulF  => {
                asm.push(format!("\tmul.f r0 r2 r3"));
            }

            Instructions::Lsh   => {
                asm.push(format!("\tlsh r0 r2 r3"));
            }

            Instructions::Rsh   => {
                asm.push(format!("\trsh r0 r2 r3"));
            }

            Instructions::Xor   => {
                asm.push(format!("\txor r0 r2 r3"));
            }

            Instructions::BwOr  => {
                asm.push(format!("\tor r0 r2 r3"));
            }

            Instructions::BwAnd => {
                asm.push(format!("\tand r0 r2 r3"));
            }

            Instructions::Not   => {
                asm.push(format!("\tnot r0 r2"));
            }

            Instructions::Ne{ label }    => {
                asm.push(format!("\tbne r2 r3 {}", label));
            }

            Instructions::Eq{ label }    => {
                asm.push(format!("\tbeq r2 r3 {}", label));
            }

            Instructions::Lt{ label }    => {
                asm.push(format!("\tblt r2 r3 {}", label));
            }

            Instructions::LtSpecific{ label, lhs ,rhs }    => {
                asm.push(format!("\tblt r{} r{} {}", lhs, rhs, label));
            }

            Instructions::Gt{ label }    => {
                asm.push(format!("\tbgt r2 r3 {}", label));
            }

            Instructions::Gte{ label }   => {
                asm.push(format!("\tbgt r2 r3 {}\n", label));
                asm.push(format!("\tbeq r2 r3 {}", label));
            }

            Instructions::Lte{ label }   => {
                asm.push(format!("\tblt r2 r3 {}\n", label));
                asm.push(format!("\tbeq r2 r3 {}", label));
            }

            Instructions::NeF{ label }   => {
                asm.push(format!("\tbne.f r2 r3 {}", label));
            }

            Instructions::EqF{ label }   => {
                asm.push(format!("\tbeq.f r2 r3 {}", label));
            }

            Instructions::LtF{ label }   => {
                asm.push(format!("\tblt.f r2 r3 {}", label));
            }

            Instructions::GtF{ label }   => {
                asm.push(format!("\tbgt.f r2 r3 {}", label));
            }

            Instructions::GteF{ label }  => {
                asm.push(format!("\tbgt.f r2 r3 {}\n", label));
                asm.push(format!("\tbeq.f r2 r3 {}", label));
            }

            Instructions::LteF{ label }  => {
                asm.push(format!("\tblt.f r2 r3 {}\n", label));
                asm.push(format!("\tbeq.f r2 r3 {}", label));
            }
        }
    }

    let file = match File::create("generated.lvm") {
        Ok(f) => { f },
        Err(e) => { 
            eprintln!("Unable to open file for writing : {}", e);
            std::process::exit(1);
        }
    };

    let mut file = LineWriter::new(file);

    for item in asm.iter() {

      //  print!("{}", item);

        if let Err(e) = file.write_all(item.as_bytes()) {
            eprintln!("Failed while writing file : {}", e);
            std::process::exit(1);
        }
    }

  //  println!("");

    drop(file);

    match lvm::assembler::assemble(String::from("generated.lvm"), Vec::new()) {
        Ok(bytes) => {
            return bytes;
        }
        Err(e) => {
            eprintln!("Error assembling generated instructions : {}", e);
            std::process::exit(1);
        }
    }
}