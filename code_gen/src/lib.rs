
extern crate derive_more;

extern crate lvm;

// mod blocks;
// pub mod builder;

mod optimizer;
pub use optimizer::optimize_instruction_set;
mod generator;
pub use generator::generate_bytecode;

use derive_more::{ Display };

#[derive(Display, Debug, Clone)]
pub enum Instructions {

    #[display(fmt = "ASM Comment : {}", data)]
    Comment{ data: String },

    #[display(fmt = "Call {}", name)]
    Call{ name: String },

    #[display(fmt = "Label {}", name)]
    Label{ name: String },

    #[display(fmt = "Goto {}", label)]
    Goto{ label: String },

    #[display(fmt = "Stb from r{} to addr in r{}", source, dest)]
    Stb{ source: u8, dest: u8},

    #[display(fmt = "Ldb from r{} to addr in r{}", source, dest)]
    Ldb{ source: u8, dest: u8},

    Return,

    #[display(fmt = "Expand Stack r{}", source)]
    ExpandStack{ source: u8 },

    MarkStack,
    ResetStack,

    #[display(fmt = "Frame Pointer to r{}", dest)]
    Fp{ dest: u8 },

    #[display(fmt = "Update Frame Pointer from r{}", source)]
    Ufp{ source: u8 },

    #[display(fmt = "Stack Size to r{}", dest)]
    StackSize{ dest: u8 },

    #[display(fmt = "Push Word from r{}", source)]
    PushWord{ source: u8 },

    #[display(fmt = "Push Half from r{}", source)]
    PushHalf{ source: u8 },

    #[display(fmt = "Push byte from r{}", source)]
    PushByte{ source: u8 },

    #[display(fmt = "Pop Word to r{}", dest)]
    PopWord{ dest: u8 },

    #[display(fmt = "Pop Half Word to r{}", dest)]
    PopHalf{ dest: u8 },

    #[display(fmt = "Pop byte to r{}", dest)]
    PopByte{ dest: u8 },

    #[display(fmt = "MovValue '{}' into r{}", source, dest)]
    MovValue{ dest: u8, source: u64},

    #[display(fmt = "MovExact '{}' into r{}", source, dest)]
    MovExact{ dest: u8, source: String},

    #[display(fmt = "MovAddress '{}' into r{}", source, dest)]
    MovAddress{ dest: u8, source: String},

    #[display(fmt = "MovRegister r{} to r{}", source, dest)]
    MovRegister{ dest: u8, source: u8},

    #[display(fmt = "Store Stack Word from r{} to address in r{}", source, dest_addr)]
    StoreStackWord{ dest_addr: u8, source: u8},

    #[display(fmt = "Store Stack Half Word from r{} to address in r{}", source, dest_addr)]
    StoreStackHalf{ dest_addr: u8, source: u8},

    #[display(fmt = "Store Stack Byte from address in r{} to {}", source, dest_addr)]
    StoreStackByte{ dest_addr: u8, source: u8},

    #[display(fmt = "Load Stack Word from address in r{} to r{}", source_addr, dest)]
    LoadStackWord{ source_addr: u8, dest: u8},

    #[display(fmt = "Load Stack Half Word from address in r{} to {}", source_addr, dest)]
    LoadStackHalf{ source_addr: u8, dest: u8},

    #[display(fmt = "Load Stack Byte from address in r{} to {}", source_addr, dest)]
    LoadStackByte{ source_addr: u8, dest: u8},

    #[display(fmt = "Store memory byte from r{} to address in r{}", source, dest_addr)]
    StoreMemoryByte{ dest_addr: u8, source: u8},

    #[display(fmt = "Load memory byte from address in r{} to {}", source_addr, dest)]
    LoadMemoryByte{ source_addr: u8, dest: u8},

    #[display(fmt = "Assert Equal r{} and r{}", lhs, rhs)]
    AssertEq{ lhs: u8, rhs: u8 },

    #[display(fmt = "Assert Not Equal r{} and r{}", lhs, rhs)]
    AssertNe{ lhs: u8, rhs: u8 },

    // Destination for all mathematical operations will be register 0
    // Source for all mathematical operations will be register 2, and register 3 if standard operation
    //      and only register 2 if its a unary operation - See note in blocks.rs
    Add,

    #[display(fmt = "Add specific : lhs: r{} , rhs: r{}, dest: {}", lhs, rhs, dest)]
    AddSpecific{ dest: u8, lhs: u8, rhs: u8},
    Sub,
    Div,
    Mul,
    Pow,
    Mod,

    AddF,
    SubF,
    DivF,
    MulF,

    Lsh  ,
    Rsh  ,
    Xor  ,
    BwOr ,
    BwAnd,
    Not,
    Ne      { label: String },
    Eq      { label: String },
    Lt      { label: String },
    Gt      { label: String },
    Gte     { label: String },
    Lte     { label: String },
    NeF     { label: String },
    EqF     { label: String },
    LtF     { label: String },
    GtF     { label: String },
    GteF    { label: String },
    LteF    { label: String },
    #[display(fmt = "Branch LT specific : lhs: r{} , rhs: r{}, label: {}", lhs, rhs, label)]
    LtSpecific      { label: String, lhs: u8, rhs: u8 },
}
