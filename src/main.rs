
#[macro_use]
extern crate verbosity;

extern crate clap;
use clap::{Arg, App, SubCommand};

use std::path::Path;
use std::ffi::OsStr;
use std::fs::File;
use std::process::exit;
use std::io::prelude::*;

extern crate source;
extern crate compiler;

extern crate lvm;

fn main() {
    let matches = App::new("Umbra Compiler")
                            .version("0.0.0")
                            .author("Bosley <bosley117@gmail.com>")
                            .about("Compiles Umbra source code to LVM bytecode")
                            .arg(Arg::with_name("v")
                                .short("v")
                                .multiple(true)
                                .help("Sets the level of verbosity (v | vv | vvv)"))
                            .subcommand(SubCommand::with_name("build")
                                .about("Compile Umbra to LVM bytecode")
                                .version("0.1")
                                .author("Josh B. <bosley117@gmail.com>")
                                .arg(Arg::with_name("target")
                                    .short("t")
                                    .long("target")
                                    .help("Target file for compilation")
                                    .takes_value(true)
                                    .required(true))
                                .arg(Arg::with_name("include")
                                    .short("i")
                                    .long("include")
                                    .help("Files to include in build")
                                    .takes_value(true)
                                    .multiple(true))
                                .arg(Arg::with_name("output")
                                    .short("o")
                                    .long("output")
                                    .help("Specifies an output file name")
                                    .takes_value(true)))
                            .subcommand(SubCommand::with_name("run")
                                .about("Runs a compiled LVM file")
                                .version("0.1")
                                .author("Josh B. <bosley117@gmail.com>")
                                .arg(Arg::with_name("target")
                                    .short("t")
                                    .long("target")
                                    .help("Target file to load and execute")
                                    .takes_value(true)
                                    .required(true)))
                          .get_matches();

    let v = match matches.occurrences_of("v") {
        0 => { verbosity_off!() },
        1 => { verbosity_low!() },
        2 => { verbosity_medium!() },
        _ => { verbosity_high!() },
    };

    // Show verbosity if not set to off
    verbosity_show!(&v);

    if let Some(matches) = matches.subcommand_matches("run") {

        // Run a file
        if let Some(value) = matches.value_of("target") {
            println!("Run : {}", value);

            let program_byte_code = read_assembled_bytes(value);

            run_application(program_byte_code);
            std::process::exit(0);
        }
    }


    if let Some(matches) = matches.subcommand_matches("build") {

        // Get the target
        if let Some(target) = matches.value_of("target") {

            // Build any includes that might be floating around
            let mut include_files: Vec<&str> = Vec::new();

            if let Some(_) = matches.value_of("include") {
                include_files = matches.values_of("include").unwrap().collect();
            }

            vmed!(&v, vfunc!(), format!("Target : {} | includes : {:?}", target, include_files).as_str());

            let program_byte_code = compile_target(&v, target, include_files);

            if let Some(output_name) = matches.value_of("output") {

                write_assembled_bytes(output_name, &program_byte_code);
                std::process::exit(0);
            } else {

                write_assembled_bytes(target, &program_byte_code);
                std::process::exit(0);
            }
        }
    }

    println!("No input file given. Use -h for help.");
    std::process::exit(1);
}

fn compile_target(v: &verbosity::Verbosity, target: &str, includes: Vec<&str>) -> Vec<u8> {

    let mut source_manager = source::Source::new(v);

    // First item loaded is the entry file, so we send that off first
    if let Some(error) = source_manager.load_source_file(&String::from(target)) {
        println!("Error: {}", error);
        std::process::exit(1);
    };

    // Load all of the included files into the source manager
    for include in includes.iter() {
        if let Some(error) = source_manager.load_source_file(&String::from(include.clone())) {
            println!("Error: {}", error);
            std::process::exit(1);
        };
    }

    let mut compiler = compiler::Compiler::new(v, &source_manager);

    match compiler.compile() {
        Some(result) => {

            println!("Time elapsed: {:?}", result.time_elapsed);
            println!("Bytes generated: {}", result.byte_code.len());

            return result.byte_code;
        },
        None => {
            std::process::exit(1);
        }
    }
}

// Write the bytes out to a file after assemble
//
fn write_assembled_bytes(given_path: &str, byte_code: &Vec<u8>) {
    let output_name : String;
    let mut file_name = Path::new(given_path).file_name().unwrap().to_str().unwrap();
    if let Some(extension) = Path::new(file_name)
                            .extension()
                            .and_then(OsStr::to_str) {
        file_name = &file_name[0..(file_name.len() - extension.len())];
        output_name = format!("{}out", file_name);
    } else {
        output_name = format!("{}.out", file_name);
    }

    let mut file = match File::create(output_name.clone()) {
        Ok(f) => f,
        Err(_) => {
            eprintln!("Error : Unable to open '{}' for writing", output_name);
            exit(1);
        }
    };

    if let Err(e) = file.write_all(byte_code) {
        eprintln!("Error writing file : {}", e);
        exit(1);
    }
}

//  Read in a suspected byte code file
//
fn read_assembled_bytes(given_path: &str) -> Vec<u8> {

    // Open the file
    let mut file = match File::open(given_path){
        Ok(f)  => f,
        Err(e) => {
            eprintln!("Error opening file '{}'", e);
            std::process::exit(1);
        }
    };

    // Read file into byte vector
    let mut buffer = Vec::<u8>::new();
    if let Err(e) = file.read_to_end(&mut buffer) {
        eprintln!("Error reading in file '{}'", e);
        std::process::exit(1);
    };

    return buffer;
}

//  Run the application
//
fn run_application(byte_code: Vec<u8>) {

    // Attempt to start a vm with the data
    let mut virtual_machine = match lvm::Vm::new(byte_code) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error creating VM : {}", e);
            std::process::exit(1);
        }
    };

    loop {
        match virtual_machine.step() {
            Ok(completed) => {
                if completed {
                    std::process::exit(0);
                }
            },
            Err(e) => {
                println!("{}", e);
                std::process::exit(1);
            }
        }
    }
}