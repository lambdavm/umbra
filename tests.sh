semantic_passes="test_code/semantic_passes"
semantic_failures="test_code/semantic_failures"
regression="test_code/regression"
regression_name="regression_test"
EXT=".um"

function semantic_test {

    # Go into the folder that holds the test cases
    cd ${1}

    for i in *.um; do
        [ -f "$i" ] || break
        
        echo -e "<TESTING : ${i} | Expecting return code ${2}"

        # Execute the test in release mode
        ../../target/release/umbra build -t "${i}"

        # Ensure the exit code is what we expect
        if [ $? -ne "${2}" ]; then
            echo -e "\n\t<TEST FAILURE>\n"
            exit 1
        fi

        echo -e "<PASS>\n"
    done

    # Return to initial dir
    cd -
}

function regression_tests {

    # Go into the folder that holds the test cases
    cd ${regression}

    for i in *.um; do
        [ -f "$i" ] || break
        
        echo -e "<TESTING : ${i} >"

        # Execute the test in release mode
        ../../target/release/umbra build -t "${i}" -o "${regression_name}"

        # Ensure the exit code is what we expect
        if [ $? -ne 0 ]; then
            rm generated.lvm
            echo -e "\n\t<TEST FAILURE - BUILD FAIL : ${i} >\n"
            exit 1
        fi

        rm generated.lvm

        ../../target/release/umbra run   -t "${regression_name}".out

        # Ensure the exit code is what we expect
        if [ $? -ne 0 ]; then

            rm "${regression_name}".out
            echo -e "\n\t<TEST FAILURE - EXEC FAIL : ${i} >\n"
            exit 1
        fi
        
        rm "${regression_name}".out

        echo -e "<PASS : ${i} >\n"
    done

    # Return to initial dir
    cd -
}

semantic_test ${semantic_failures} 1
semantic_test ${semantic_passes}   0
regression_tests

cd ${semantic_passes} 
rm ./*.out 
cd -
