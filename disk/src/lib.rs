
use std::fs;
use std::path::Path;

#[macro_use]
extern crate verbosity;

use verbosity::Verbosity;

/// Create a directory based on a given path iff the directory doesn't exist
pub fn create_directory(v: &Verbosity, path: &Path) -> bool {

    vlow!(&v, vfunc!(), path.to_str().unwrap());

    if path.exists() {
        vlow!(&v, vfunc!(), "Given directory already exists");
        return false;
    }

    if fs::create_dir_all(path).is_err() {
        vlow!(&v, vfunc!(), "Unable to create directory");
        return false;
    }
    true
}

/// Write a vector of data out to a file
pub fn write_file(v: &Verbosity, path: &Path, data: &Vec<String>) -> bool {

    vlow!(&v, vfunc!(), path.to_str().unwrap());

    for item in data.iter() {
        if fs::write(path, item).is_err() {
            vlow!(&v, vfunc!(), "Unable to write item to disk");
            return false;
        }
    }
    true
}

/// Read file contents
pub fn read_whole_file(v: &Verbosity, path: &Path) -> Option<String> {

    vlow!(&v, vfunc!(), path.to_str().unwrap());

    if let Ok(data) = fs::read_to_string(path) {
        return Some(data);
    }
    None
}

/// Get all of the paths in a directory
pub fn collect_directory(v: &Verbosity, dir: &Path, items: &mut Vec<std::fs::DirEntry>) -> bool {

    if dir.is_dir() {

        let disk_object = match fs::read_dir(dir) {
            Ok(object) => { object }
            Err(_)     => { 
                vmed!(&v, vfunc!(), "Unable to read directory!");
                return false 
            }
        };

        for entry in disk_object {

            let entry = entry.unwrap();
            let path  = entry.path();
            if path.is_dir() {
                if !collect_directory(v, &path, items) {
                    return false;
                }
            } else {
                vhigh!(&v, vfunc!(), 
                    format!("Pushing directory item : {}", 
                    &entry.path().to_str().unwrap()).as_str());
                items.push(entry);
            }
        }
    }
    true
}