
# Data Types

|   Type    |     Bytes     |   Note |
|---        |---            |---     |
|    int    |       8       |  Basic signed integer |
|   float   |       8       |  Basic signed float|
|    char   |       8       |  UTF-8 Char |
|   struct  |    Variable   |  A structure containing multiple data types |
|    nil    |       0       |  This type is not assignable. It is to indicate 'no return' for a function |

The data types in Umbra are still being created, but the first iteration of Umbra will have the above types. 
Once the above types are being generated and proper bytecode files are being generated / tested the language will expand to to enable the use of pointers of each given type, and then there will be LVM specific types that will enable us to direct hardware devices.


# Includes

Including files with the 'i' argument will populate them as potential sources for items included by file name. If there are duplicate file names included, the first one will be used. Include statements only look at the file name and extension, not the whole path. This may be updated in the future but it is sufficient for now. 

Any data brought in from an include is brought into the same scope as the file that includes it. That means functions and structs from imported files need to have unique names. 

**Example:**
```
    include "my_file.um"
```

Duplicate includes will be ignored which enables the wild-west of importing without import cycles. 
All include statements are evaluated before any code generation which means no forward declaring will be required. By the time the code is being evaluated everything that exists in a global scope will be reachable by everything else.

# Comments

Comments in Umbra are marked by '//'. As of yet there are no block comments, and there might never be unless it really seems necessary. 


# Functions

All functions take the following form:
```
def <function name> ( <parameters> ) -> <return type> {
    <statements>
}
```

In any program there must exist a main function. Right now the return type of the main function is lost, and there are no way to bring parameters into main, but in he future this will change. 

**Example**

```
def main() -> nil {

    // This is a return. 
    <- nil;
}
```

# Variables

Unlike many popular languages there is no variable shadowing, and everything in a function shares a scope. The declaring of variables and setting of existing variables is done so very verbosely and conversions between types is not yet permitted. There is no simple int->float or float->int conversion yet. 

To declare a new variable use the keyword "let", and to update the variable use the keyword "set"

**Example**

```

def main() -> nil {

    let x : int = 55 + 3 + 2;
    let y : int = x + 40;

    set y = y + 200;

    <- nil;
};

```

# Structs

Struct definitions must happen outside of functions and take the following form:

```

struct <struct name> {

    <member name>: <type>,
    <member name>: <type>,
    ..
    ..
    <member name>: <type>
};

```

All member names must be unique and as of now, structures can not contain members that are themselves structures. This was done to simplify the first stages of development. Using sub structures is planned for later expansion.

## Declaring a structure

```
let <struct name> : struct <struct name> = <struct name> { 
        <expression>,
        <expression>,
        <expression>
    };
```

The declaration of structures is quite verbose, and it is so by design. I like verbosity. Each expression in the list must have a resulting data type that matches the type of its corresponding member.

**Example Structure Use**

```
struct MyStruct {
    my_int: int,
    my_float: float,
    my_char: char
};

def main() -> nil {

    let my_struct : struct MyStruct = MyStruct { 
        0,
        3.33,
        'f'
    };

    set my_struct.my_char = 'g';

    let copy : int = my_struct.my_int;

    <- nil;
}

```

**Example 2**

```
struct MyStruct {
    x: float,
    y: float,
    offset: float
};

def build_struct(given_x: float, given_y: float) -> struct MyStruct {

    let new_struct : struct MyStruct = MyStruct {
        given_x,
        given_y,
        0.0
    };

    <- new_struct; 
}

def main() -> nil {

    let ns : struct MyStruct = build_struct( 3.55, 4.44 );

    set ns.y = 24.00;

    <- nil;
}
```


# Built In Items

While the language is being developed, to ensure that everything is happening under the hood that we intend, a couple of functions have been baked into the language. These functions can be accessed like a standard function, with the exception that they must be preceded by "::" and that they can take any primitive types as their arguments. 

| Function      |   Argument Count  |       About
|---            |---                |---
|  assert_eq    |          2        |       Ensures that any two expressions are equal. This is evaluated at run time
|  assert_ne    |          2        |       Ensures that any two expressions are NOT equal. This is evaluated at run time.


---

**Example**

```

def main()  -> nil {

    let x : int = 5;
    

    ::assert_eq( x, 5 );

    <- nil;
}

```

# Control Flow

Umbra has some basic control flow worked into it. For loops, while loops, and if statements. As it stands, each of them will only accept integer and float conditionals. 

## For Loops

The statements of a for loop are verbose. It takes the form:

```
    for ( <assignment>; <condition>; <update>; ) {
        
        <statements+>
    }
```

Each of the members that comprise the for loop specification must end in a ';' as they would on their own anywhere else they might be seen in the code. 

**Example**

```
    for( let i : int = 0; i < 3; set i = i + 1; ) {

    }
```

## While Loops 

While loops take a single conditional argument. Similar to what might be seen in the 'C' language.

```

    while ( <condition >) {

        <statements+>
    }

```

The loop body will only be executed if the condition is true... as one would expect.

**Example**

```
    let some_int : int = 0;

    while ( some_int < 100 ) {

        set some_int = some_int + 1;
    }
```

## If Statements

The if statements in Umbra take on a normal every-day if-elif-else look. Not much to say about them.

```
    if ( <condition> ) {

        <statements+>

    } elif ( <condition> ) {

        <statements+>

    } elif ( <condition> ) {

        <statements+>

    } else {

        <statements+>
        
    }
```

Just like should be expected, If statements can take the form of the singular "if" or have an arbitrary number of "elif" following... and if you neeed it you can also place "else" at the end. 

**Example**

```
    if ( 0 ) {
        
    } elif ( 0 ) {
        
    } elif ( 0 ) {
        
    } elif ( 0 ) {
        
    } else {

        // HEY WE MADE IT
    }
```